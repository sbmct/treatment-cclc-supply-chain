#Most demos have use python code in a virtualenv, created by make
VENVDO=$(PWD)/scripts/venvdo

#Other things that change per demo
WEB_TIME=web/modules/runner/static/runner/js/demo_time.json

all: initialize

deps: $(PWD)/venv/bin/activate

$(WEB_TIME): lib/python/supply_chain/demo_time.json
	cp $^ $@

$(PWD)/venv/bin/activate: requirements.txt
	test -d $(PWD)/venv/bin || virtualenv $(PWD)/venv
	$(VENVDO) pip install --upgrade pip
	$(VENVDO) pip install -r $^

FLUORO_DIR=$(PWD)/share/
MEDIA_FILES=$(FLUORO_DIR)/static_images $(FLUORO_DIR)/videos
BUILT_TOPO_FILES=$(FLUORO_DIR)/queue_metrics/data/topo/queue_data.yaml \
	    $(FLUORO_DIR)/queue_metrics/data/topo/queue_obs.yaml \
        $(FLUORO_DIR)/supply_chain/data/topo/worldports.yaml \
	    $(FLUORO_DIR)/supply_chain/data/topo/suppliers.yaml \
	    $(FLUORO_DIR)/supply_chain/data/topo/stores.yaml \
	    $(FLUORO_DIR)/supply_chain/data/topo/stores_obs.yaml \
	    $(FLUORO_DIR)/supply_chain/data/topo/dist_centers.yaml \
	    $(FLUORO_DIR)/supply_chain/data/topo/shipping_routes.yaml
TOPO_FILES=$(BUILT_TOPO_FILES) \
        $(FLUORO_DIR)/shopper_paths/data/topo/shopper_paths.yaml \
        $(FLUORO_DIR)/media/data/topo/flickr.yaml \
        $(FLUORO_DIR)/media/data/topo/news.yaml \
        $(FLUORO_DIR)/media/data/topo/youtube.yaml
DEMO_DEST=$(PWD)/share/demographics/zippy


data_files: deps $(TOPO_FILES) $(WEB_TIME) $(MEDIA_FILES)

initialize: data_files
	rsync -avzhessh conduce_xfer@xfer.mct.io:pub/demo_data/retail/zippy/* $(DEMO_DEST)

$(FLUORO_DIR)/static_images:
	mkdir -p $(FLUORO_DIR)/static_images
$(FLUORO_DIR)/videos:
	mkdir -p $(FLUORO_DIR)/videos

$(FLUORO_DIR)/supply_chain/data/topo/worldports.yaml:
	$(VENVDO) python share/supply_chain/data/retail_supply_chain.py
$(FLUORO_DIR)/supply_chain/data/topo/suppliers.yaml:
	$(VENVDO) python share/supply_chain/data/retail_supply_chain.py
$(FLUORO_DIR)/supply_chain/data/topo/stores.yaml:
	$(VENVDO) python share/supply_chain/data/retail_supply_chain.py
$(FLUORO_DIR)/supply_chain/data/topo/stores_obs.yaml:
	$(VENVDO) python share/supply_chain/data/retail_supply_chain.py
$(FLUORO_DIR)/supply_chain/data/topo/dist_centers.yaml:
	$(VENVDO) python share/supply_chain/data/retail_supply_chain.py
$(FLUORO_DIR)/supply_chain/data/topo/shipping_routes.yaml:
	$(VENVDO) python share/supply_chain/data/retail_supply_chain.py


$(FLUORO_DIR)/media/data/topo/flickr.yaml:
	$(VENVDO) python share/media/data/flickr_historic.py -f $@

$(FLUORO_DIR)/media/data/topo/news.yaml:
	$(VENVDO) python share/media/data/news.py -f $@

$(FLUORO_DIR)/media/data/topo/youtube.yaml:
	$(VENVDO) python share/media/data/youtube_historic.py -f $@

# data protein is included in repo;
# don’t remake tweets to avoid over-querying Twitter API
# $(FLUORO_DIR)/media/data/topo/tweets.yaml:
# 	$(VENVDO) python share/media/data/twitter.py -f $@


$(FLUORO_DIR)/queue_metrics/data/topo/queue_data.yaml:
	$(VENVDO) python share/queue_metrics/data/queue_data.py -f $@

$(FLUORO_DIR)/queue_metrics/data/topo/queue_obs.yaml:
	$(VENVDO) python share/queue_metrics/data/queue_flow.py -f $@

$(FLUORO_DIR)/shopper_paths/data/topo/shopper_paths.yaml:
	$(VENVDO) python share/shopper_paths/data/shopper_paths.py -f $@

neat:
	rm -f $(WEB_TIME)
	rm -f $(BUILT_TOPO_FILES)

clean: neat
	rm -fr $(PWD)/venv

superclean: clean
	rm -f $(TOPO_FILES)

