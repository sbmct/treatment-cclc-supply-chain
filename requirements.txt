pysluice >= 1.1.5
python-dateutil
google-api-python-client
docopt
unicodecsv
tweepy
cplasma >= 0.2
PyYAML
Pillow
svg.path
conduce_pycairo
