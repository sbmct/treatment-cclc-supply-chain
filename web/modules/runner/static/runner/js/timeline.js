function Timeline(slider){
    const TO_SLUICE = 'tcp://localhost/edge-to-sluice';
    const FROM_SLUICE = 'tcp://localhost/sluice-to-heart';

    const TIME_CHANGE = ['sluice', 'prot-spec v1.0', 'request', 'set-time'];

    function fmt_date(x) {
    if (undefined != x) {
        var d = new Date(x * 1000);
        // console.log(d.toString());
        // console.log(d);
        // console.log(x);
        return d.toString();
    } else {
        console.log('Whatever');
        return 'Unknown';
    }
    }

    var minimum_time = 0.0, maximum_time = 0.0, cur_time = 0.0;
    var rate = 1.0;

    function time_change(ing) {
    Plasma
        .Hose(TO_SLUICE)
        .Deposit ({ descrips : TIME_CHANGE,
            ingests  : ing });
    }

    function sliderchange(evt) {
        var myrate, paused;
        if (slider.attr('rate') !== undefined) {
            myrate = parseFloat(slider.attr('rate'));
            paused = (myrate == 0);
        } else {
            myrate = rate;
            paused = false;
        }
        time_change({ pause: paused, time: parseInt(slider.attr('value')), rate: myrate });
    }

    //timeslider event listeners
    slider.change(function(evt){sliderchange(evt)});
    document.getElementById('range-set').addEventListener("input",
        function(evt){
            sliderchange(evt)
        }
    );

    Plasma
    .Hose (FROM_SLUICE)
    .Match (['sluice', 'prot-spec v1.0', 'psa', 'heartbeat'])
    .Await (function (p) {
        $('#time-rate').text(rate = p.ingests.time.rate);
        $('#time-current').text(fmt_date(cur_time = p.ingests.time.current));
        slider.attr('value', cur_time);
        if (undefined != p.ingests.time.min) {
            // minimum_time = p.ingests.time.min;
            $('#time-minimum').text(fmt_date(slider.attr('min')));
        }
        if (undefined != p.ingests.time.max) {
            // maximum_time = p.ingests.time.max
            $('#time-maximum').text(fmt_date(slider.attr('max')));
        }
    });

    var masterTime = null;

    var PARSEABLE_KEYS = ['min', 'max' ,'value'];
    $.ajax({
        url: "static/runner/js/demo_time.json",
        async: true,
        dataType: 'json',
        error: function(resp) {
            console.log(resp);
            console.log('demo_time does not exist or is not valid JSON')
        },
        success: function (data) {
            masterTime = {};
            for (var key in data) {
                masterTime[key] = data[key];
                //convert PARSEABLE_KEYS to ints if they are strings
                if (masterTime[key] && typeof masterTime[key] === "object") {
                    for (var i = 0; i < PARSEABLE_KEYS.length; i++) {
                        var subkey = PARSEABLE_KEYS[i];
                        var myTime = masterTime[key][subkey]
                        if (typeof myTime == 'string' || myTime instanceof String) {
                            //TODO: Better date support (times are funny)
                            masterTime[key][subkey] = (Date.parse(myTime))/1000;
                        }
                    }
                }
            }
            console.log("setting master time to ", masterTime);
        }
    });

    function timeStart() { return masterTime.start_time + 1200; }
    function monthlyEnd() { return masterTime.monthly_end - 1200; }
    function dailyStart() { return masterTime.daily_start + 1200; }
    function dailyEnd() { return masterTime.daily_end - 1200; }
    function hourlyStart() { return masterTime.hourly_start + 1200; }
    function hourlyEnd() { return masterTime.hourly_end - 1200; }

    this.setLiveTime = function(){
        //TODO: Figure out how to disable the slider
        //tried slider.slider( "option", "disabled", true );
        //and slider.slider("disable"); and a few others
        //with no luck
        time_change({'live': true, 'rate': 1});
    }

    this.setTimeRangeMonthly = function(){
        slider.attr('min', timeStart());
        slider.attr('max', monthlyEnd());
        slider.attr('value', timeStart());
        slider.change();
    };
    this.setTimeRangeDaily = function(){
        var min_time = dailyStart();
        var max_time = dailyEnd();
        // var max_time = min_time + 86400;
        slider.attr('min', min_time);
        slider.attr('max', max_time);
        slider.attr('value', min_time + (max_time - min_time) / 2.0);
        slider.change();
    };
    this.setTimeRangeHourly = function(){
        var min_time = hourlyStart();
        var max_time = hourlyEnd();
        // var max_time = min_time + 3600;
        slider.attr('min', min_time);
        slider.attr('max', max_time);
        slider.attr('value', min_time + (max_time - min_time) / 2.0);
        slider.change();
    };


    //TODO: Implement a set rate (and maybe a pause function or just rate =0 --> paused)
    //TODO: Implement set time method
    this.has_named_time = function(key) {
        return masterTime.hasOwnProperty(key);
    }

    this.set_named_time = function(key){
        if (!this.has_named_time(key)) {
            return;
        }
        cfg = masterTime[key];
        slider.attr('min', cfg.min);
        slider.attr('max', cfg.max);
        if (cfg.hasOwnProperty('value')) {
            slider.attr('value', cfg.value);
        } else {
            slider.attr('value', cfg.min + (cfg.max - cfg.min) / 2.0);
        }
        if (cfg.hasOwnProperty('rate')) {
            slider.attr('rate', cfg.rate);
        } else {
            slider.attr('rate', 1.0);
        }
        slider.change();
    };


    this.set_demo_time = function() {
        if (masterTime === null) {
            var self = this;
            setTimeout(function() {self.set_demo_time()}, 100);
            return;
        }
        if (masterTime.hasOwnProperty('start')) {
            this.set_named_time('start');
        } else {
            this.setTimeRangeMonthly();
        }
    }

    return this;
};
