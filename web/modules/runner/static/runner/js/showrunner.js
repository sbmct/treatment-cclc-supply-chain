function load_runner_config(runner) {

    var timeline = runner.timeline;

    const slc_s = 'sluice';
    const prs_s = 'prot-spec v1.0';
    const req_s = 'request';
    const rmf_s = 'remove-all-fluoro'
    const zom_s = 'zoom';

    var legend_url = document.location.toString().replace(/#.*$/, '')+'static/Roundels%20Legend.html';

    var templates = { };
    Plasma.Hose('sluice-to-edge')
        .Match([slc_s, prs_s, 'psa', 'fluoroscope-templates'])
        .Await(function(p) {
            templates = { };
            var tmpls = p.ingests.templates;
            for(var i = tmpls.length - 1; i >= 0; i--) {
                templates[tmpls[i].name] = tmpls[i];
            }
        });
    Plasma.Hose('edge-to-sluice').Deposit({
        descrips: [slc_s, prs_s, req_s, 'fluoroscope-templates'],
        ingests: { }
    });

    var timeline = new Timeline($('#range-set'));

    var show_time = function() {
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s, runner.req_s, 'web'],
            ingests: {
                'windshield': [
                    {
                        'feld': 'main',
                        'url': 'http://localhost:7787/launchpad/static/time.html',
                        'name': 'time',
                        'size': [0.3, 0.06],
                        'loc': [-0.46, 0.32]
                    }
                ]
            }
        })
    }

    var sq_directory_config = {
        "type": "texture",
        "disable-notifications": true,
        "ignore-movement": true,
        "image-is-static": true,
        "daemon": "static_static_images/" + "Squared Directory.png",
        "name": "Squared Directory",
    };

    var oos_config = {
        "daemon": "TemporalFinancials",
        "name": "OOS Overlay",
        "attributes": [
            {
                "name": "opacity",
                "contents": [
                    {
                        "display-text": "25%",
                        "selected": 0,
                        "name": "0.25"
                    },
                    {
                        "display-text": "50%",
                        "selected": 0,
                        "name": "0.50"
                    },
                    {
                        "display-text": "75%",
                        "selected": 0,
                        "name": "0.75"
                    },
                    {
                        "display-text": "100%",
                        "selected": 1,
                        "name": "1.00"
                    }
                ],
                "selection-type": "exclusive"
            },
            {
                "name": "style",
                "contents": [
                    {
                        "display-text": "OOS Overlay",
                        "selected": 1,
                        "name": "oos_overlay/zippy/stock_level_decay"
                    }
                ],
                "selection-type": "exclusive"
            }
        ],
        "disable-notifications": true,
        "update-frequency": 2.0,
        "type": "texture",
    }

    function clone(obj) {
        if(obj == null) { return null }
        if(typeof obj == 'string') { return obj+'' }
        if(typeof obj == 'number') { return obj+0 }
        if(typeof obj == 'boolean') { return obj }
        if(obj instanceof Array) {
            var n = obj.length;
            var dest = new Array(n);
            for(var i = 0; i < n; i++) {
                dest[i] = clone(obj[i]);
            }
            return dest;
        }
        var dest = { };
        for(var key in obj) {
            dest[key] = clone(obj[key]);
        }
        return dest;
    }

    function edit_from_template(name, attrs) {
        var tmpl = clone(templates[name]);
        for(var i = 0; i < tmpl.attributes.length; i++) {
            var attr = tmpl.attributes[i];
            if(attrs.hasOwnProperty(attr.name)) {
                if(attr['selection-type'] == 'inclusive') {
                    var v = {};
                    if(attrs[attr.name] == null) {
                        // noop
                    } else if(!(attrs[attr.name] instanceof Array)) {
                        v[attrs[attr.name]] = true;
                    } else {
                        for(var j = 0; j < attrs[attr.name].length; j++) {
                            v[attrs[attr.name][j]] = true;
                        }
                    }
                    for(var j = 0; j < attr.contents.length; j++) {
                        if(v[attr.contents[j].name]) {
                            attr.contents[j].selected = 1;
                        } else {
                            attr.contents[j].selected = 0;
                        }
                    }
                } else {
                    for(var j = 0; j < attr.contents.length; j++) {
                        if(attr.contents[j].name == attrs[attr.name]) {
                            attr.contents[j].selected = 1;
                        } else {
                            attr.contents[j].selected = 0;
                        }
                    }
                }
            }
        }
        return tmpl;
    }

    function shipping_routes_with_distros() {
        var f = edit_from_template('Shipping routes', { 'enabled-kinds': ['shipping-route', 'supplier-route', 'integrator-route'] });
        runner.add_fluoro_args('Shipping routes', {
            'fluoro-config': f,
            bl: [-17, -300.0],
            tr: [60.5, 30.0]
        });
    }

    function zoom_to_store(zoom) {
        runner.bookmark(37.3543244, -121.8243902, zoom);
    }
    function pacific_zoom(){
        runner.bookmark(29.29, -101, 4.00); // zoom to supply chain
    }
    function pause(){
        runner.PlasmaDeposit('tcp://localhost/edge-to-sluice',
            {
                descrips: ['sluice', 'prot-spec v1.0', 'request', 'set-time'],
                ingests: {
                    'pause': true
                }
            });
    }
    function set_rate(rate){
        runner.PlasmaDeposit('tcp://localhost/edge-to-sluice',
            {
                descrips: ['sluice', 'prot-spec v1.0', 'request', 'set-time'],
                ingests: {
                    'pause': false,
                    'rate': rate
                }
            });
    }
    function act1(){
        timeline.set_named_time('healthy_historic');
        national();
        runner.clear_fluoroscopes();
        toner();
    }
    function act2(){
        timeline.set_named_time('ports');
        //national();
        runner.clear_fluoroscopes();
        toner();
        ports();
        setTimeout(national, 200);
    }
    function act3(){
        timeline.set_named_time('supply_chain')
        national();
        runner.clear_fluoroscopes();
        toner();
        ports();
        suppliers();
        shipping_routes();
    }
    function act4(){
        timeline.set_named_time('planning');
        runner.clear_fluoroscopes();
        //shipping_routes();
        full_supply_chain();
        stores_fluoro();
        westcoast();
        toner();
    }
    function national(){
        runner.bookmark(38.58, -95.14, 6.0); // zoom to full US
    }
    function solution(){
        runner.bookmark(42.03, -94.77, 7.59);
    }
    function westcoast(){
        runner.bookmark(37.3543244, -121.8243902, 20.0); // zoom to westcoast
    }
/*    function slowdown(){
        timeline.set_named_time('ports');
        ports();
    }*/
    function ports(){
        runner.add_fluoro_args("Ports", { bl: [16, -252.0],
                                     tr: [55.5, 30.0] });
    }
    function suppliers(){
        runner.add_fluoro_args("Suppliers", { bl: [-5, -300.0],
                                     tr: [50.5, -200.0] });
        runner.add_fluoro_args("Suppliers", { bl: [26, -30.0],
                                     tr: [65.5, 50.0] });
    }
    function dist_centers(){
        runner.add_fluoro_args("Distribution Centers", { bl: [16, -140.0],
                                        tr: [55.5, -45.0] });
    }
    function media(){
        runner.clear_fluoroscopes();
        national();
        timeline.set_named_time('ports')
        runner.add_fluoro_args("Media", { bl: [-40, -272.0],
                                     tr: [55.5, 30.0] });
        runner.add_fluoroscope("Ports");
    }
    function national_stores(){
        timeline.set_named_time('healthy_historic');
        runner.bookmark(38.58, -95.14, 6.0); // zoom to full US
        stores();
    }
    function stores_fluoro() {
        runner.add_fluoro_args("Stores", { bl:[16.779532282639192,-138.85270198932983],
                                           tr:[53.594171408275614,-59.589812261109806] });
    }
    function stores_under_chain(){
        runner.clear_fluoroscopes();
        timeline.set_named_time('supply_chain');
        stores_fluoro();
        //shipping_routes();
        full_supply_chain();
    }
    function stores(){
        runner.add_fluoroscope("Stores");
    }
    /// Local
    function region_with_roundels(){
        timeline.set_named_time('ok_health_historic');
        //runner.bookmark(37.5273244, -121.8243902, 400);
        zoom_to_store(600.0);
        runner.clear_fluoroscopes();
        runner.add_fluoro_args("Stores", { bl: [15.02438,-162.949219],
                                     tr: [51.886664,-47.8125] });
    }
    /// Neighborhood
    function show_neighborhood(){
        timeline.set_named_time('healthy_historic');
        neighborhood();
    }
    function neighborhood(){
        runner.bookmark (37.32726828166011, -121.88233421542854, 2643.6295626010865);
        runner.add_fluoro_args("Stores", { bl: [15.02438,-162.949219],
                                     tr: [51.886664,-47.8125] });
    }
    function city(){
        timeline.set_named_time('planning');
        var sq_dir = runner.get_fluoro_by_name('Squared Directory');
        console.debug('sq_dir = %o', sq_dir);
        if(sq_dir) {
            //var callout = sq_dir.callout;
            //sq_dir.callout = null;
            if(sq_dir.callout) {
                sq_dir.callout.width = 0.0;
            }
            delete(sq_dir.bounds);
            if(sq_dir.SlawedQID instanceof Array) {
                sq_dir.SlawedQID = { 'json_class': 'unt8_array', 'v': sq_dir.SlawedQID };
            }
            console.debug('updating sq_dir = %o', sq_dir);
            runner.update_fluoro_config(sq_dir);
        }
        runner.bookmark (37.32726828166011, -121.88233421542854, 2643.6295626010865);
    }

    /// Store
    function good_store_level(){
        timeline.set_named_time('healthy_historic');
        zoom_to_store(80000.0); // zoom into/through store roof;
    }
    function bad_store_level(){
        timeline.set_named_time('planning');
        zoom_to_store(80000.0); // zoom into/through store roof;
    }
    function show_oos_overlay() {
        runner.clear_fluoroscopes();
        show_time();
        timeline.set_named_time('store_stock_fast_rate');
        oos_overlay();
    }
    function oos_overlay(){
        runner.add_fluoro_args("OOS Overlay", {"fluoro-config": oos_config});
    }
    function paths(){
        runner.add_fluoroscope("Entity Paths");
    }
    function tripwire(){
        runner.add_fluoro_args("TripWire",
                        { bl: [37.35293344966172, -121.82684639044389],
                          tr: [37.354374286465514, -121.82355220806051] });
    }
    function queue_metrics(){
        runner.add_fluoro_args("Queue Metrics",
                        { bl: [37.3533, -121.8259],
                          tr: [37.3540, -121.8246] });
    }

    function sq_directory(){
        timeline.set_named_time('planning');
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s, runner.req_s, 'new-fluoro-instance'],
            ingests: {
                'fluoro-config': sq_directory_config,
                'windshield': true,
                'bl': [30.518716467255718, -170.6034544865517],
                'tr': [39.433478732182657, -161.02297576166828]
            }
        });
    }
    function sq_video(){
        timeline.set_named_time('planning');
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s, runner.req_s, 'new-fluoro-instance'],
            ingests: {
                'fluoro-config': {
                    "type": "texture",
                    "disable-notifications": true,
                    "ignore-movement": true,
                    "image-is-static": true,
                    "daemon": "static_static_images/" + "Squared Video.png",
                    "name": "Squared Video",
                },
                'windshield': true,
                'bl': [33.253550451407271, -160.52151811666894],
                'tr': [43.884621559099216, -138.69465113903541]
            }
        });
    }
    function ships_manifest(){
        runner.bookmark(38.862502947889709,-111.26809352094142, 20);
        timeline.set_named_time('supply_chain');
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s, runner.req_s, 'new-fluoro-instance'],
            ingests: {
                'fluoro-config': {
                    "type": "texture",
                    "disable-notifications": true,
                    "ignore-movement": true,
                    "image-is-static": true,
                    "daemon": "static_static_images/" + "Ships Manifest.png",
                    "name": "Ships Manifest",
                    callout: {
                        loc: [33.766666666666666,-118.183333333333337,0.0],
                        color: {
                            type: 'SoftColor',
                            value: [0.4, 0.4, 0.4, 1],
                        },
                        width: 0.25,
                        background: {
                            width: 0.05,
                        },
                    }
                },
                'windshield': true,
                'bl':[19.232000623625261, -241.75717078123321],
                'tr':[54.925598350033006, -156.36105024459553]
            }
        });
    }

    function roundel_legend(){
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s, runner.req_s, 'web'],
            ingests: {
                windshield: [{
                    name: 'Roundels Legend',
                    feld: 'left',
                    url: legend_url,
                    visible: true,
                    size: [0.75, 0.9],
                    loc: [0.0, 0.0]
                }]
            }
        });
        /*
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s, runner.req_s, 'new-fluoro-instance'],
            ingests: {
                'fluoro-config': {
                    "type": "texture",
                    "disable-notifications": true,
                    "ignore-movement": true,
                    "image-is-static": true,
                    "daemon": "Roundels Legend",
                    "name": "Roundels Legend",
                },
                'windshield': true,
                'bl':[37.150382596320564, -123.28008950977731],
                'tr':[37.551682534349543, -122.57334480677062]
            }
        });
        */
    }

    function compare(){
        west_coast_store();
        east_coast_store();
    }

    function east_coast_store(){
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s, runner.req_s, 'new-fluoro-instance'],
            ingests: {
                'fluoro-config': {
                    "type": "texture",
                    "disable-notifications": true,
                    "ignore-movement": true,
                    "image-is-static": true,
                    "daemon": "static_static_images/" + "cclc_healthy_store.png",
                    "name": "east_coast_store",
                },
              'bl':
              [ 25.206607,
              -70.398437],
              'tr':
              [ 40.410203,
              -52.311523]
            }
        });
    }

    function west_coast_store(){
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s, runner.req_s, 'new-fluoro-instance'],
            ingests: {
                'fluoro-config': {
                    "type": "texture",
                    "disable-notifications": true,
                    "ignore-movement": true,
                    "image-is-static": true,
                    "daemon": "static_static_images/" + "cclc_sick_store.png",
                    "name": "west_coast_store",
                },
              'bl':
              [ 29.812912,
              -144.800195],
              'tr':
              [ 45.805025,
              -126.801172]
            }
        });
    }

    function show_full_supply_chain() {
        timeline.set_named_time('supply_chain');
        runner.clear_fluoroscopes();
        full_supply_chain();
        ports();
    }

    function full_supply_chain(){
        shipping_routes_with_distros();
        dist_centers();
        suppliers();
    }

    function change_map_substrate(choice){
        var mapquest = false;
        var toner = false;
        var matte = false;
        if(choice == "mapquest"){
            mapquest = true;
        } else if(choice == "toner"){
            toner = true;
        } else if(choice == "matte"){
            matte = true;
        }
        var substrate = {
            "background": {
                "selection-type": "exclusive",
                "contents": [
                    {
                        "display-text": "Mapquest",
                        "name": 'mapquest',
                        "selected": mapquest
                    },
                    {
                        "display-text": "Watercolor",
                        "name": 'watercolor',
                        "selected": false
                    },
                    {
                        "display-text": "Toner Lines",
                        "name": 'toner-lines',
                        "selected": false
                    },
                    {
                        "display-text": "Toner",
                        "name": 'toner',
                        "selected": toner
                    },
                    {
                        "display-text": "Matte dark",
                        "name": 'sluice/mercator/world-mercator-4096-04.png',
                        "selected": matte
                    },
                    {
                        "display-text": "Shiny dark",
                        "name": 'sluice/mercator/world-mercator-4096-03.png',
                        "selected": false
                    },
                    {
                        "display-text": "Blue Marble",
                        "name": 'sluice/mercator/blue-marble.png',
                        "selected": false
                    }
                ]
            }
        }
        runner.update_fluoros_attrs("Map", substrate);
    }

    function mapquest(){
        change_map_substrate("mapquest");
    }

    function toner(){
        change_map_substrate("toner");
    }

    function legend(){
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s,
                       runner.req_s, 'web'],
            ingests: {
                'windshield': [
                    {
                        'feld': 'right',
                        'url': 'http://dictionary.reference.com/browse/legend',
                        'name': 'roundel legend',
                        'background': [1.0, 1.0, 1.0, 1.0],
                        'size': [0.9, 0.9]
                    }
                ]
            }
        });
    }

    function display_article(article, bl, tr, origin, color) {
        if(color == "red"){
            color = [0.9961,0.3725,0.3725,1];
        }
        else if(color == "green"){
            color = [0.4314,0.8941,0.2980,1];
        }
        else if(color == "yellow"){
            color = [0.9593,0.9098,0.3412,1];
        }
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s, runner.req_s, 'new-fluoro-instance'],
            ingests: {
                'fluoro-config': {
                    "type": "texture",
                    "disable-notifications": true,
                    "ignore-movement": true,
                    "image-is-static": true,
                    "daemon": "static_static_images/" + article + ".png",
                    "name": article,
                    callout: {
                        loc: origin,
                        color: {
                            type: 'SoftColor',
                            value: color,
                        },
                        width: 0.1,
                        background: {
                            width: 0.02,
                        },
                    },
                },
              'bl':bl,
              'tr':tr
            }
        });
    }

    function articles(){
        display_article("tweetG",[43.9239051547058,-120.31954136732044],[56.73568322827478,-88.232627367320376],[33.7669444, -118.1883333, 0.0],"green");
        display_article("tweetB2",[43.191169191890602,-240.06531280812146],[56.176971790670351,-207.9783988081214],[22.243907, -245.84546, 0.0],"red");
        display_article("tweetB3",[41.207907441652907,-36.813808668690335],[54.65746075150016,-4.7268946686902975],[36.162663799999997, -86.781601600000002, 0.0],"red");
        display_article("tweetB4",[14.283783895470863,-17.805266624392122],[32.729135121002152,14.281647375607873],[33.748995399999998, -84.387982399999999, 0.0],"red");
        display_article("tweetB1",[41.574079817533722,-80.658311500773323],[54.938824281055666,-48.571397500773351],[38.9071923, -77.0368707, 0.0],"red");
        display_article("tweetN",[16.77287906362378,-191.11317126468853],[34.875790146003922,-159.02625726468833],[47.0378741, -122.9006951, 0.0],"yellow");
        display_article("newsG",[38.265316446562416,-183.46805571700344],[55.535572084919174,-163.59084952084024],[61.2403, -149.8861, 0.0],"green");
        display_article("newsB1",[15.880948097306481,-145.02193840960081],[36.511450567406506,-127.02291540960081],[34.0522300, -118.2436800, 0.0],"red");
        display_article("newsB2",[13.476094447795015,-70.960156472041788],[34.487599435772061,-49.961133472041773],[33.7669444, -118.1883333, 0.0],"red");
        display_article("newsB3",[14.20228531735458,-220.28613908567812],[39.319786829527487,-198.17248304428185],[13.0, -238.0, 0.0],"red");
        display_article("newsB4",[33.768804224714209,22.834996290154088],[53.846331433814463,47.197820618759792],[52.3081, 4.7642, 0.0],"red");
    }

    function webex_video(name, fname, bl, tr){
        runner.add_fluoro_args('webex',{
            'fluoro-config': {
                "name": name,
                "type": "video",
                "disable-notifications": true,
                "vid_loc": {
                    'type': 'file',
                    'path': 'webex/'+fname+'.mp4'
                }
            },
            'windshield': true,
            'bl': bl,
            'tr': tr
        });
    }
    function webex(){
        var bl = [33.253550451407271, -160.52151811666894];
        var tr = [43.884621559099216, -138.69465113903541];
        webex_video('webex', 'slider_webex', bl, tr);
    }

    function store_activity() {
        timeline.set_named_time('store');
        runner.clear_fluoroscopes();
        paths();
        tripwire();
        queue_metrics();
        oos_overlay();
        show_time();
    }

    function shipping_options(){
        var filters = {
            'contents': [
                { 'name': 'true',
                  'display-text': 'All',
                  'selected': false
                },
                { 'name': 'observation(n, "rank", t) == "0"',
                  'display-text': 'Primary Routes',
                  'selected': false
                },
                { 'name': 'parseInt(observation(n, "rank", t)) > 0',
                  'display-text': 'Secondary Routes',
                  'selected': false
                }
            ],
            'selection-type': 'inclusive'
        };
        runner.update_fluoros_attrs("Shipping routes", { "dynamic-filters-whitelist": filters });
    }

    function shipping_routes(){
        runner.add_fluoro_args("Shipping routes", { bl: [-17, -300.0],
                                            tr: [60.5, 30.0] });
    }
/*    function blue_shipping_routes(){
        timeline.set_named_time('supply_chain');
        suppliers();
        shipping_routes();
    }
*/
    function rewind_time_ports(){
        timeline.set_named_time('ports_historical_fast_rate');
    }
    function rewind_time_store(){
        timeline.set_named_time('store_stock_fast_rate');
    }
    function backlog(){
        timeline.set_named_time('ports');
        runner.add_fluoroscope("Shipping Backlog");
        show_time();
        runner.bookmark(33.700746999999999,-118.20342899999999,1100);
    }

    function chain_dies(){
        timeline.set_named_time('supply_chain_forecast_fast_rate');
    }

    function solutionAct() {
        runner.clear_fluoroscopes();
        timeline.set_named_time('resolution');
        stores_fluoro();
        //shipping_routes();
        full_supply_chain();
        ports();
        show_time();
        solution();
    }
/*    function load_shipping_routes_and_stores() {
        timeline.set_named_time('resolution');
        stores_fluoro();
        //shipping_routes();
        full_supply_chain();
        ports();
        show_time();
    }*/
    function the_end(){
        timeline.set_named_time('resolution_fast_rate');
    }

    function webex2(){
        var bl = [25.427848602378479, -212.57013837583114];
        var tr = [54.483192381720833, -150.28002068812145];
        webex_video('webex2', 'slider_webex', bl, tr);
    }

    function act6() {
        runner.clear_fluoroscopes();
        timeline.set_named_time('resolution');
        solution();
    }
    function slides(slide_num){
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s, runner.req_s, 'web'],
            ingests: {
                windshield: [{
                    name: 'Slides',
                    feld: 'main',
                    url: 'http://localhost:7787/launchpad/static/slide'+slide_num+'.html',
                    visible: true,
                    size: [0.75, 0.75],
                    loc: [0.0, 0.0]
                }]
            }
        });
    }
    function slides1(){
        slides(1);
    }
    function slides2(){
        slides(2);
    }
    function slides3(){
        slides(3);
    }
    function slides4(){
        slides(4);
    }
    function dummy(){
        //placeholder
    }

   //return methods to call
    runner_object = {
        'act1': ['Stores', act1],
        'a1scene1': ['Show Stores', national_stores],
        'a1scene2': ['Zoom to City', show_neighborhood],
        'a1scene3': ['Zoom into Store', good_store_level],
        'a1scene4': ['Live Store Activity', store_activity],
        'a1scene5': ['Zoom to Regional', region_with_roundels],
        'a1scene6': ['Roundel Legend', roundel_legend],
        'act2': ['Port Slowdown', act2],
        'a2scene1': ['Backlog of Ships', backlog],
        'a2scene2': ['Rewind Time', rewind_time_ports],
        'a2scene3': ['Zoom to National', national],
        'a2scene4': ['Media Icons', media],
        'a2scene5': ['Media Articles', articles],
        'act3': ['Supply Chain', act3],
        'a3scene1': ['Ships Manifest', ships_manifest],
        'a3scene2': ['Supply Chain', show_full_supply_chain],
        'a3scene3': ['Stores', stores_under_chain],
        'a3scene4': ['Time Slides Forward', chain_dies],
        'act4': ['See Effect in Store', dummy],
        'a4scene1': ['Zoom to Unhealthy Store', bad_store_level],
        'a4scene2': ['Bad Store Data', show_oos_overlay],
        'a4scene3': ['Zoom to Solution', solution],
        'act5': ['Solution', solutionAct],
        'a5scene1': ['Squared Video', webex2],
        'a5scene2': ['Rerouting', the_end],
        //'act5': ['Options', act4],
        //'a5scene1': ['Squared Directory', sq_directory],
        //'a5scene2': ['Squared Video', webex],
        //'a5scene3': ['Zoom to City', city],
        //'act6': ['Slides', act6],
        //'a6scene1': ['Slide 1', slides1],
        //'a6scene2': ['Slide 2', slides2],
        //'a6scene3': ['Slide 3', slides3],
        //'a6scene4': ['Slide 4', slides4],
        //'a6scene7': ['Roundel Legend', roundel_legend],
    }
    return runner_object;

}
