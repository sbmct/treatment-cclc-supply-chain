var LaunchPad = {
    EDGE_TO_SLUICE: Plasma.Hose('edge-to-sluice'),
    SLUICE_TO_EDGE: Plasma.Hose('sluice-to-edge'),
    slc_s: 'sluice',
    prs_s: 'prot-spec v1.0',
    req_s: 'request',
    psa_s: 'psa',
};

LaunchPad.v1descrips = function() {
    var descrips = [LaunchPad.slc_s, LaunchPad.prs_s];
    return descrips.concat($A(arguments));
};

LaunchPad.deposit = function(pool, descrips, ingests) {
    if(typeof pool == 'string') {
        pool = Plasma.Hose(pool);
    }
    if(descrips == null) {
        descrips = [];
    }
    if(ingests == null) {
        ingests = {};
    }
    console.debug('deposit (%o, %o)', descrips, ingests);
    return pool.Deposit({ descrips: descrips, ingests: ingests });
};

LaunchPad.appendMetabolizer = function(pool, descrips, handler) {
    console.debug('awaiting %o', descrips);
    if(typeof pool == 'string') {
        pool = Plasma.Hose(pool);
    }
    return pool.Match(descrips).Await(handler);
};

LaunchPad.awaitOne = function(pool, descrips, handler) {
    var ix = null;
    var metabo = function(p) {
        //console.debug('awaitOne got one');
        Plasma.unAwait(ix);
        handler(p);
    };
    ix = LaunchPad.appendMetabolizer(pool, descrips, metabo);
    return ix;
};

LaunchPad.setTime = function(cfg) {
    //console.debug('setTime');
    LaunchPad.deposit(
        LaunchPad.EDGE_TO_SLUICE,
        LaunchPad.v1descrips(LaunchPad.req_s, 'set-time'),
        cfg
    );
};

LaunchPad.getFluoroList = function() {
    return new Promise(function(resolve, reject) {
        LaunchPad.awaitOne(
            LaunchPad.SLUICE_TO_EDGE,
            LaunchPad.v1descrips(LaunchPad.psa_s, 'fluoroscopes'),
            resolve
        );
        LaunchPad.deposit(
            LaunchPad.EDGE_TO_SLUICE,
            LaunchPad.v1descrips(LaunchPad.req_s, 'fluoroscopes')
        );
    });
};

LaunchPad.getTemplateList = function() {
    return new Promise(function(resolve, reject) {
        LaunchPad.awaitOne(
            LaunchPad.SLUICE_TO_EDGE,
            LaunchPad.v1descrips(LaunchPad.psa_s, 'fluoroscope-templates'),
            resolve
        );
        LaunchPad.deposit(
            LaunchPad.EDGE_TO_SLUICE,
            LaunchPad.v1descrips(LaunchPad.req_s, 'fluoroscope-templates')
        );
    });
};

LaunchPad.getAtlasView = function() {
    return new Promise(function(resolve, reject) {
        LaunchPad.awaitOne(
            LaunchPad.SLUICE_TO_EDGE,
            LaunchPad.v1descrips(LaunchPad.psa_s, 'atlas-view'),
            resolve
        );
        LaunchPad.deposit(
            LaunchPad.EDGE_TO_SLUICE,
            LaunchPad.v1descrips(LaunchPad.req_s, 'atlas-view')
        );
    });
};

LaunchPad.makeStandardBounds = function(zoom) {
    var clat = zoom.lat;
    var clon = zoom.lon;
    var z = zoom.zoom;
    var max = Math.atan(Math.sinh(Math.PI)) * 180.0 / Math.PI;
    var lat2norm = function(lat) {
        if(lat > max) { lat = max }
        else if(lat < -1 * max) { lat = -1 * max }
        lat *= Math.PI / 180.0;
    return (1.0 + Math.log((Math.sin(lat) + 1.0) / Math.cos(lat)) / Math.PI) / 2.0 - 0.5;
    };
    var norm2lat = function(norm) {
        norm += 0.5;
        norm = Math.min(1.0, Math.max(0.0, norm));
        return Math.atan(Math.sinh(((2.0 * norm) - 1.0) * Math.PI)) * 180.0 / Math.PI;
    };
    var t = norm2lat(lat2norm(clat) + 0.5 / z);
    var b = norm2lat(lat2norm(clat) - 0.5 / z);
    var h = lat2norm(t) - lat2norm(b);
    var w = h * 16 / 9;
    var norm2lon = function(norm) {
        norm += 0.5;
        return (2.0 * norm - 1.0) * 180.0;
    };
    var l = norm2lon(clon/360.0 - w/2);
    var r = norm2lon(clon/360.0 + w/2);
    return {
        bl: [b, l],
        tr: [t, r]
    };
};

LaunchPad.Runner = function(cfg, rows, cols) {
    this.cfg = cfg;
    if(rows == null) { rows = 6 }
    if(cols == null) { cols = 7 }
    this.rows = rows;
    this.cols = cols;
    this.timeline = new Timeline($('#range-set'));
    this.timeline.set_demo_time();
    this.acts = [];
    var self = this;
    if(cfg && cfg.acts != null) {
        cfg.acts.each(function(act) { self.addAct(act) });
    }
    this.currentAct = null;
    return this;
};

LaunchPad.Runner.prototype.addAct = function(act) {
    if(!(act instanceof LaunchPad.Act)) {
        act = new LaunchPad.Act(this, act);
    } else {
        act.n = this.acts.length;
    }
    this.acts.push(act);
    return act;
};

LaunchPad.Runner.prototype.render = function() {
    var self = this;
    var tbl = $('table.play')[0].tBodies[0];
    for(var i = 0; i < this.rows; i++) {
        var act = this.acts[i];
        if(act == null) {
            act = new LaunchPad.Act(this, {});
            act.n = i;
        }
        act.render(tbl, self.cols);
    }
};

LaunchPad.Runner.prototype.zoom = function(lat, lon, level, interp) {
    if(interp == null) { interp = 5.0 }
    LaunchPad.deposit(
        LaunchPad.EDGE_TO_SLUICE,
        LaunchPad.v1descrips(LaunchPad.req_s, 'zoom'),
        { lat: lat, lon: lon, level: level, 'interp-time': interp }
    );
};

// promise
LaunchPad.Runner.prototype.addFluoro = function(f, attrs, bl, tr) {
    // then(function(fluoro) {})
    var cfg = { };
    if(typeof f == 'string') {
        cfg['fluoro-config'] = Object.clone(this.templates[f], true);
    } else {
        if(f.type) {
            cfg['fluoro-config'] = Object.clone(f, true);
            if (f.hasOwnProperty('windshield')) {
                cfg['windshield'] = f.windshield;
            }
        } else {
            cfg['fluoro-config'] = Object.clone(this.templates[f.name], true);
        }
    }
    if(cfg['fluoro-config'].type == 'web') {
        return new Promise(function(resolve, reject) {
            LaunchPad.awaitOne(
                LaunchPad.SLUICE_TO_EDGE,
                LaunchPad.v1descrips(LaunchPad.psa_s, 'new-fluoro-instance'),
                function(p) { resolve(p.ingests) }
            );
            LaunchPad.deposit(
                LaunchPad.EDGE_TO_SLUICE,
                LaunchPad.v1descrips(LaunchPad.req_s, 'web'),
                { 'windshield': [ cfg['fluoro-config'] ] }
            );
        });
    }
    if(attrs != null) {
        cfg['fluoro-config'] = this.setFluoroAttrs(cfg['fluoro-config'], attrs);
    }
    if(bl && tr) {
        cfg.bl = bl;
        cfg.tr = tr;
    }
    return new Promise(function(resolve, reject) {
        LaunchPad.awaitOne(
            LaunchPad.SLUICE_TO_EDGE,
            LaunchPad.v1descrips(LaunchPad.psa_s, 'new-fluoro-instance'),
            function(p) { resolve(p.ingests) }
        );
        LaunchPad.deposit(
            LaunchPad.EDGE_TO_SLUICE,
            LaunchPad.v1descrips(LaunchPad.req_s, 'new-fluoro-instance'),
            cfg
        );
    });
};

LaunchPad.Runner.prototype.setFluoroAttrs = function(f, attrs) {
    //console.debug('setting %o attrs: %o', f.name, attrs);
    var cfg = Object.clone(f, true);
    if(!cfg.attributes) {
        //console.error('no attributes for %o', f);
        return cfg;
    }
    //console.debug('attrs = %o', cfg.attributes);
    var self = this;
    cfg.attributes.each(function(attr) {
        if(!attrs.hasOwnProperty(attr.name)) {
            //console.debug('not modifying %o', attr.name);
            return;
        }
        var val = attrs[attr.name];
        //console.debug('setting %o to %o', attr.name, val);
        if(attr['selection-type'] == 'inclusive') {
            //console.debug('inclusive');
            if(val == null) {
                val = [];
            } else if(!(val instanceof Array)) {
                val = [val];
            }
            var vals = { };
            val.each(function(v) { vals[v] = true });
            attr.contents.each(function(v) {
                if(vals[v.name] || vals[v['display-text']]) {
                    v.selected = 1;
                } else {
                    v.selected = 0;
                }
            });
        } else {
            //console.debug('exclusive');
            var found = false;
            attr.contents.each(function(v, i) {
                //console.debug('checking %o', v.name);
                if(!found && v.name == val) {
                    found = true;
                    v.selected = 1;
                    //console.debug('set %o to %o (%o)', attr.name, v.name, i);
                } else {
                    v.selected = 0;
                }
            });
            if(!found) {
                attr.contents.every(function(v) {
                    if(v['display-text'] == val) {
                        v.selected = true;
                        found = true;
                        return false;
                    }
                    return true;
                });
                if(!found && attr.contents.length > 0) {
                    attr.contents[0].selected = true;
                }
            }
        }
    });
    return cfg;
};

LaunchPad.Runner.prototype.fluoroUnchanged = function(oldf, newf) {
    if(!oldf.attributes) {
        if(newf.attributes) { return false }
        return true;
    } else if(!newf.attributes) {
        return false;
    }
    //console.debug('comparing %o to %o', oldf.attributes, newf.attributes);
    if(oldf.attributes.length != newf.attributes.length) {
        return false;
    }
    return newf.attributes.every(function(attr, i) {
        var oattr = oldf.attributes[i];
        if(attr.name != oattr.name) {
            return false;
        }
        if(attr.contents.length != oattr.contents.length) {
            return false;
        }
        return attr.contents.every(function(v, j) {
            var ov = oattr.contents[j];
            if(v.name != ov.name) {
                return false;
            }
            if(v.selected && !ov.selected) {
                return false;
            }
            if(!v.selected && ov.selected) {
                return false;
            }
            return true;
        });
    });
};

LaunchPad.Runner.prototype.moveFluoro = function(f, bl, tr) {
};

LaunchPad.Runner.prototype.markDirty = function(key) {
    if(key == null) {
        this.dirty = {
            templates: true,
            fluoroscopes: true,
            atlas: true,
            atlasView: true
        };
    } else {
        if(this.dirty == null) {
            this.markDirty();
        }
        this.dirty[key] = true;
    }
    return this.dirty;
};

LaunchPad.Runner.prototype.markClean = function(key) {
    if(this.dirty == null) { this.markDirty() }
    this.dirty[key] = false;
    return this.dirty;
};

LaunchPad.Runner.prototype.isClean = function(key) {
    if(this.dirty == null) { return false }
    if(this.dirty[key] == null) { return false }
    if(this.dirty[key]) { return false }
    return true;
};

// promise
LaunchPad.Runner.prototype.loadAtlasView = function() {
    var self = this;
    return new Promise(function(resolve, reject) {
        if(self.isClean('atlasView') && self.atlasView != null) {
            resolve();
            return;
        }
        LaunchPad.getAtlasView().then(function(p) {
            if(!p || !p.ingests) {
                reject();
                return;
            }
            self.atlasView = p.ingests;
            self.markClean('atlasView');
            resolve();
        });
    });
};

// promise
LaunchPad.Runner.prototype.getTemplateByName = function(name) {
    // then(function(tmpl) {})
    // catch(function(name) {})
    var self = this;
    return new Promise(function(resolve, reject) {
        self.loadTemplates().then(function() {
            if(self.templates[name] == null) {
                reject(name);
                return;
            }
            resolve(self.templates[name]);
        });
    });
};

// promise
LaunchPad.Runner.prototype.loadTemplates = function() {
    // then(function() {})
    // catch(function() {})
    var self = this;
    return new Promise(function(resolve, reject) {
        if(self.isClean('templates') && self.templates != null) {
            resolve();
            return;
        }
        LaunchPad.getTemplateList().then(function(p) {
            if(!p || !p.ingests || !p.ingests.templates) {
                reject();
                return;
            }
            var templates = { };
            var n = p.ingests.templates.length;
            for(var i = 0; i < n; i++) {
                var tmpl = p.ingests.templates[i];
                templates[tmpl.name] = tmpl;
            }
            self.templates = templates;
            self.markClean('templates');
            resolve();
        });
    });
};

// promise
LaunchPad.Runner.prototype.getFluoroByName = function(name, n) {
    // then(function(fluoro) {})
    // catch(function(name, n) {})
    var self = this;
    if(n == null) {
        n = 0;
    }
    return new Promise(function(resolve, reject) {
        self.loadFluoros().then(function() {
            if(self.fluoros[name] == null || self.fluoros[name][n] == null) {
                reject(name, n);
                return;
            }
            resolve(self.fluoros[name][n]);
        });
    });
};

// promise
LaunchPad.Runner.prototype.loadFluoros = function() {
    // then(function() {})
    // catch(function() {})
    var self = this;
    return new Promise(function(resolve, reject) {
        if(self.isClean('fluoroscopes') && self.fluoros != null) {
            resolve();
            return;
        }
        LaunchPad.getFluoroList().then(function(p) {
            if(!p || !p.ingests || !p.ingests.fluoroscopes) {
                reject();
                return;
            }
            self.fluoroOrder = p.ingests.fluoroscopes;
            self.fluoros = { };
            self.atlas = null;
            var n = p.ingests.fluoroscopes.length;
            for(var i = 0; i < n; i++) {
                var f = p.ingests.fluoroscopes[i];
                if(f.type == 'atlas' && self.atlas == null) {
                    self.atlas = f;
                    self.markClean('atlas');
                }
                if(self.fluoros[f.name] == null) {
                    self.fluoros[f.name] = [];
                }
                self.fluoros[f.name].push(f);
            }
            self.markClean('fluoroscopes');
            resolve();
        });
    });
};

/*
LaunchPad.Runner.prototype.configureFluoro = function(f, n, attrs) {
    return new Promise(function() {
        if(f.SlawedQID) {
            this.
        } else {
            var name = null;
            if(typeof f == 'string') {
                name = f;
            } else {
                name = f.name;
            }
        }
        this.loadTemplates().then(function() {
            this.loadFluoros().then(function() {
                var cfg = null;
                var name = null;
                var n = 0;
                if(typeof f == 'string') {
                    name = f;
                } else if(f instanceof Array) {
                    name = f[0];
                    n = f[1];
                } else {
                    name = f.name;
                }
            });
        });
    });
};
*/

LaunchPad.Runner.prototype.updateFluoroConfig = function(f) {
    var cfg = Object.clone(f);
    delete(cfg.bounds);
    delete(cfg.bl);
    delete(cfg.tr);
    LaunchPad.deposit(
        LaunchPad.EDGE_TO_SLUICE,
        LaunchPad.v1descrips(LaunchPad.req_s, 'configure-fluoro'),
        cfg
    );
};

LaunchPad.Runner.prototype.reorderFluoros = function(scopes) {
    return LaunchPad.getFluoroList();
    return new Promise(function(resolve, reject) {
        LaunchPad.awaitOne(
            LaunchPad.SLUICE_TO_EDGE,
            LaunchPad.v1descrips(LaunchPad.psa_s, 'fluoroscopes'),
            resolve
        );
        LaunchPad.deposit(
            LaunchPad.EDGE_TO_SLUICE,
            LaunchPad.v1descrips(LaunchPad.req_s, 'reorder-z-axis'),
            { 'scopes': scopes }
        );
    });
};

LaunchPad.Runner.prototype.removeFluoro = function(f) {
    var ing;
    if(typeof f == 'string') {
        ing = { 'name': f };
    } else {
        ing = { 'SlawedQID': { 'json_class': 'unt8_array', 'v': f.SlawedQID } };
        //console.debug('remove fluoro %o', f);
    }
    LaunchPad.deposit(
        LaunchPad.EDGE_TO_SLUICE,
        LaunchPad.v1descrips(LaunchPad.req_s, 'remove-fluoro-instance'),
        ing
    );
};

LaunchPad.Runner.prototype.ensureFluoros = function(cfg) {
    var self = this;
    this.loadAtlasView().then(function() {
        if(cfg.time) {
            if(cfg.time.time) {
                if (self.timeline.has_named_time(cfg.time.time)){
                    self.timeline.set_named_time(cfg.time.time);
                } else if(cfg.time.time == 'now') {
                    self.timeline.setLiveTime();
                } else if(cfg.time.time == 'monthly') {
                    self.timeline.setTimeRangeMonthly();
                } else if(cfg.time.time == 'daily') {
                    self.timeline.setTimeRangeDaily();
                } else if(cfg.time.time == 'hourly') {
                    self.timeline.setTimeRangeHourly();
                } else if(typeof cfg.time.time == 'string') {
                    var time = new Date(cfg.time.time).getTime() / 1000.0;
                    LaunchPad.setTime({ 'time': time });
                } else if(typeof cfg.time.time == 'number') {
                    LaunchPad.setTime({ 'time': cfg.time.time });
                }
            }
            if(cfg.time.rate) {
                var req = { 'rate': cfg.time.rate };
                req['pause'] = (cfg.time.rate == 0);
                LaunchPad.setTime(req);
            }
        }
        if(cfg.zoom) {
            if(cfg.zoom.lat == null) { cfg.zoom.lat = self.atlasView.lat }
            if(cfg.zoom.lon == null) { cfg.zoom.lon = self.atlasView.lon }
            if(cfg.zoom.zoom == null) { cfg.zoom.zoom = self.atlasView.zoom }
            self.zoom(cfg.zoom.lat, cfg.zoom.lon, cfg.zoom.zoom);
        }
        if(cfg.map) {
            self.loadFluoros().then(function() {
                var newf;
                //console.debug('atlas is toner? %o', self.atlas.attributes[2].contents[3].selected);
                if(typeof cfg.map == 'string') {
                    newf = self.setFluoroAttrs(self.atlas, { 'background': cfg.map });
                } else {
                    newf = self.setFluoroAttrs(self.atlas, cfg.map);
                }
                //console.debug('atlas is toner? %o', self.atlas.attributes[2].contents[3].selected);
                //console.debug('new atlas is toner? %o', newf.attributes[2].contents[3].selected);
                //console.debug('map configured with %o %o', cfg.map, newf);
                if(!self.fluoroUnchanged(self.atlas, newf)) {
                    self.updateFluoroConfig(newf);
                } else {
                    //console.debug('map unchanged');
                }
            });
        }
        if(cfg.fluoros) {
            self.loadTemplates().then(function() {
                self.loadFluoros().then(function() {
                    var promiseCount = 0;
                    var hasPromises = false;
                    var curOrder = Object.clone(self.fluoroOrder);
                    var newOrder = [curOrder.pull(function(f) { return f.type == 'atlas' })];
                    var fs = { };
                    var cb = function() {
                        //console.debug('newOrder = %o', newOrder);
                        var scopes = newOrder.collect(function(x) {
                            return x[0];
                        });
                        self.reorderFluoros(scopes).then(function() {
                            newOrder.each(function(x) {
                                var oldf = x[0];
                                var newf = x[1];
                                if(newf != null) {
                                    if(!self.fluoroUnchanged(oldf, newf)) {
                                        self.updateFluoroConfig(newf);
                                    }
                                    if(newf.bl || newf.tr) {
                                        self.moveFluoro(newf, newf.bl, newf.tr);
                                    }
                                }
                            });
                            curOrder.each(function(f) {
                                self.removeFluoro(f);
                            });
                        });
                    };
                    var toAdd = [];
                    cfg.fluoros.each(function(f, i) {
                        var oldf = curOrder.pull(function(xf) { return xf.name == f.name });
                        if(oldf) {
                            //console.debug('no need to add (%o) %o', i, f);
                            var newf = oldf;
                            if(f.attrs) {
                                newf = self.setFluoroAttrs(newf, f.attrs);
                            }
                            if(f.bl && f.tr) {
                                newf.bl = f.bl;
                                newf.tr = f.tr;
                            }
                            newOrder.push([oldf, newf]);
                        } else {
                            hasPromises = true;
                            promiseCount++;
                            newOrder.push(null);
                            //console.debug('need to add (%o) %o', i, f);
                            toAdd.push([f, i+1]);

                        }
                    });
                    toAdd.each(function(x) {
                        //console.debug('adding (%o) %o', x[1], x[0]);
                        var f = Object.clone(x[0]);
                        var attrs = f.attrs;
                        var bl = f.bl;
                        var tr = f.tr;
                        delete(f.attrs);
                        delete(f.bl);
                        delete(f.tr);
                        self.addFluoro(f, attrs, bl, tr).then(function(newf) {
                            newOrder[x[1]] = [newf, null];
                            //console.debug('added (%o) %o', x[1], x[0]);
                            promiseCount--;
                            if(promiseCount == 0) {
                                //console.debug('last fluoro added');
                                cb();
                            }
                        });
                    });
                    if(!hasPromises) {
                        //console.debug('no fluoros to add');
                        cb();
                    }
                });
            });
        }
    });
};

LaunchPad.Act = function(runner, cfg) {
    this.runner = runner;
    this.cfg = cfg;
    this.n = runner.acts.length;
    this.scenes = [];
    this.name    = cfg.name;
    this.map     = cfg.map;
    this.zoom    = cfg.zoom;
    this.time    = cfg.time;
    this.fluoros = cfg.fluoros;
    this.clear   = cfg.clear;
    this.handler = cfg.handler;
    if(cfg.scenes != null) {
        var n = cfg.scenes.length;
        for(var i = 0; i < n; i++) {
            this.addScene(cfg.scenes[i]);
        }
    }
    this.activeScenes = [];
    return this;
};

LaunchPad.Act.prototype.addScene = function(scene) {
    if(!(scene instanceof LaunchPad.Scene)) {
        scene = new LaunchPad.Scene(this, scene);
    } else {
        scene.n = this.scenes.length;
    }
    this.scenes.push(scene);
    return scene;
};

LaunchPad.Act.prototype.setup = function(finish) {
    var cfg = { };
    if(this.n != 0) {
        cfg = this.runner.acts[this.n - 1].setup(true);
    }
    cfg = this.mergeConfig(cfg);
    return cfg;
};

LaunchPad.Act.prototype.mergeConfig = function(cfg) {
    var newCfg = Object.clone(cfg, true);
    if(this.map) {
        if(newCfg.map == null) {
            newCfg.map = { };
        } else if(typeof newCfg.map == 'string') {
            newCfg.map = { background: newCfg.map };
        }
        if(typeof this.map == 'string') {
            newCfg.map.background = this.map;
        } else {
            for(var k in this.map) {
                newCfg.map[k] = this.map[k];
            }
        }
    }
    if(this.zoom) {
        if(newCfg.zoom == null) {
            newCfg.zoom = { };
        } else if(typeof newCfg.zoom == 'number') {
            newCfg.zoom = { zoom: newCfg.zoom };
        }
        if(typeof this.zoom == 'number') {
            newCfg.zoom.zoom = this.zoom;
        } else {
            for(var k in this.zoom) {
                newCfg.zoom[k] = this.zoom[k];
            }
        }
    }
    if(this.time != null) {
        if(newCfg.time == null) {
            newCfg.time = { };
        } else if(typeof newCfg.time == 'number' || typeof newCfg.time == 'string') {
            newCfg.time = { time: newCfg.time };
        } else if(typeof newCfg.time == 'string') {
        }
        if(typeof this.time == 'number' || typeof this.time == 'string') {
            newCfg.time.time = this.time;
        } else {
            for(var k in this.time) {
                newCfg.time[k] = this.time[k];
            }
        }
    }
    /*
    if(newCfg.time != null && typeof newCfg.time == 'string') {
        newCfg.time = { time: new Date(newCfg.time).getTime() / 1000.0 };
    }
    */
    if(this.clear) {
        newCfg.fluoros = [];
    }
    if(this.fluoros) {
        if(newCfg.fluoros == null || newCfg.fluoros.length == 0) {
            newCfg.fluoros = this.fluoros.collect(function(f) {
                if(typeof f == 'string') {
                    f = { name: f };
                }
                if(!(f.bl && f.tr) && f.type != 'web') {
                    console.debug('standardizing bounds');
                    var bounds = LaunchPad.makeStandardBounds(newCfg.zoom);
                    f.bl = bounds.bl;
                    f.tr = bounds.tr;
                    console.debug('%o = %o', f.name, bounds);
                } else {
                    console.debug('fluoro already has bounds');
                }
                return f;
            });
        } else {
            var fs = [];
            this.fluoros.each(function(f) {
                var name;
                if(typeof f == 'string') {
                    name = f;
                    f = { name: f };
                } else {
                    name = f.name;
                }
                var x = newCfg.fluoros.pull(function(xf) {
                    return xf.name == name;
                });
                if(!x && !(f.bl && f.tr) && f.type != 'web') {
                    console.debug('standardizing bounds');
                    var bounds = LaunchPad.makeStandardBounds(newCfg.zoom);
                    f.bl = bounds.bl;
                    f.tr = bounds.tr;
                } else if(!x) {
                    console.debug('bounds already set');
                } else {
                    console.debug('fluoro already exists');
                }
                fs.push(f)
            });
            newCfg.fluoros = newCfg.fluoros.concat(fs);
        }
    }
    return newCfg;
};

LaunchPad.Act.prototype.launch = function() {
    var cfg = this.setup();
    this.runner.ensureFluoros(cfg);
    if(this.handler) {
        this.handler(this.runner, this);
    }
};

LaunchPad.Act.prototype.render = function(tbl) {
    var self = this;
    var row = tbl.rows[this.n];
    if(!row) {
        row = Element.create('tr');
        tbl.insert(row);
        row.insert(Element.create('td', { 'class': 'act' }).update(Element.create('a', { 'href': '#', 'class': 'button' }).update(Element.create('div'))));
        var cell = Element.create('td', { 'class': 'scene' });
        row.insert(cell);
        for(var i = 0; i < this.runner.cols; i++) {
            cell.insert(Element.create('a', { 'href': '#', 'class': 'ebutton' }));
        }
    }
    row.cells[0].firstElementChild.firstElementChild.update(this.name);
    row.cells[0].firstElementChild.onclick = function() {
        self.runner.markDirty();
        self.launch();
        return false;
    };
    this.scenes.each(function(scene, i) {
        scene.render(row);
    });
};

LaunchPad.Scene = function(act, cfg) {
    this.act = act;
    this.cfg = cfg;
    this.n = act.scenes.length;
    this.name    = cfg.name;
    this.map     = cfg.map;
    this.zoom    = cfg.zoom;
    this.time    = cfg.time;
    this.fluoros = cfg.fluoros;
    this.clear   = cfg.clear;
    this.handler = cfg.handler;
    return this;
};

LaunchPad.Scene.prototype.mergeConfig = LaunchPad.Act.prototype.mergeConfig;

LaunchPad.Scene.prototype.setup = function() {
    var cfg = { };
    if(this.n == 0) {
        cfg = this.act.setup();
    } else {
        cfg = this.act.scenes[this.n - 1].setup();
    }
    cfg = this.mergeConfig(cfg);
    //console.debug('act %o, scene %o config = %o', this.act.n+1, this.n+1, cfg);
    return cfg;
};

LaunchPad.Scene.prototype.launch = function() {
    var cfg = this.setup();
    this.act.runner.ensureFluoros(cfg);
    if(this.handler) {
        this.handler(this.act.runner, this.act, this);
    }
};

LaunchPad.Scene.prototype.render = function(row) {
    var self = this;
    var node = $A(row.cells[1].children).findAll(function(n) {
        return n.nodeType == n.ELEMENT_NODE;
    })[this.n];
    if(!node) {
        node = Element.create('a', { 'href': '#', 'class': 'button' });
        row.cells[1].insert(node);
    }
    node.className = 'button';
    node.update('');
    node.insert(Element.create('div', { 'class': 'num' }).update(this.n+1));
    node.insert(Element.create('div').update(this.name));
    node.onclick = function() {
        self.act.runner.markDirty();
        self.launch();
        return false;
    };
};

