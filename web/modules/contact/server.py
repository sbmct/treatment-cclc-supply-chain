import sys
from flask import Blueprint, render_template, request
from emcs_admin_utils import jsplasma

contact = Blueprint('contact', __name__,
                   static_folder = 'static',
                   template_folder = 'templates')

@contact.route('/', methods=['GET', 'POST'])
def base():
    if request.method == 'GET':
        f = request.args
    else:
        f = request.form
    config = dict((k, f[k]) for k in f.keys())
    sys.stderr.write(str(config)+'\n')
    return render_template('contact.html', **config)

