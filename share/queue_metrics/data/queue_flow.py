"""queue obs data protein generator

Usage:
  queue_obs.py [-f output_filename]

Options:
  -f FILE   specify output file (defaults to pool)
"""
from docopt import docopt
import collections
import dateutil
import itertools

from supply_chain import time_ref
import sluice
from sluice.types import float64

import random

cashierCount = 15
#queueWaitTime = 5 * 60
itemsScannedPerSecond = ( 600 / float(3600) )
paymentTimeSeconds = 45
customersEnteringPerSecond = 1. / 56
utcReference = time_ref.hourly_start() - 24200
#utcReference = 1417150800 #5AM black friday 2014 GMT
#utcReference = 1417179600 #5AM black friday 2014 PST
#averageShoppingDurationSeconds = ( 40 * 60 )

queues = []
shoppers = []
satisfiedCustomers = []

def initialize_queues():
    for i in range(0, cashierCount):
        isOpen = (random.randint(1,10) > 6)
        queues.append(dict(laneNumber=(i + 1), laneOpen=isOpen, shoppers=[]))

def initialize_queue_metrics_data():
    for i in range(0, cashierCount):
        if queues[i]['laneOpen']:
            yield {
                'id': 'queue%02d' % (i + 1),
                'timestamp': utcReference,
                'attrs': {
                    'people_in_line': str(0)
                }
            }
            yield {
                'id': 'wait%02d' % (i + 1),
                'timestamp': utcReference,
                'attrs': {
                    'wait(sec)': str(0)
                }
            }

def add_to_queue(shopper, time):
    shopper['enterQueueTime'] = time
    # Let's not assign randomly -- doesn't look realistic
    #queueIdx = random.randint(0, (cashierCount - 1))
    shortestQueueLength = 1e6
    shortestQueue = []
    for queue in queues:
        shoppersInQueue = queue['shoppers']
        if len(shoppersInQueue) < shortestQueueLength and queue['laneOpen']:
            shortestQueueLength = len(shoppersInQueue)
            shortestQueue = queue

    shopper['assignedQueue'] = shortestQueue['laneNumber']
    shopper['queueLength'] = len(shortestQueue['shoppers'])
    if not shortestQueue['shoppers']:
        shopper['startCheckoutTime'] = time

    shortestQueue['shoppers'].append(shopper)

def start_next_checkout(queue, time):
    if queue:
        shopper = queue[0]
        shopper['startCheckoutTime'] = time

def shoppers_enter_store(time):
    duration = random.randint(10, 2*3600)
    items = random.randint(1, 100)

    chance = random.randint(1, 1 / customersEnteringPerSecond)

    if chance == 1 / customersEnteringPerSecond:
        shoppers.append(dict(startTime=time, shoppingDuration=duration, itemCount=items))

def shoppers_enter_queue(time):
    for shopper in shoppers:
        enterQueueTime = shopper['startTime'] + shopper['shoppingDuration']
        if enterQueueTime <= time:
            add_to_queue(shopper, time)
            shoppers.remove(shopper)

def complete_checkout(time, shoppersInQueue):
    shopperAtRegister = shoppersInQueue[0]
    checkoutDuration = shopperAtRegister['itemCount'] / itemsScannedPerSecond + paymentTimeSeconds
    finishCheckoutTime = shopperAtRegister['startCheckoutTime'] + checkoutDuration
    if finishCheckoutTime <= time:
        shopperAtRegister['finishCheckoutTime'] = time
        satisfiedCustomers.append(shopperAtRegister)
        shoppersInQueue.pop(0)
        start_next_checkout(shoppersInQueue, time)

def shoppers_checkout(time):
    for queue in queues:
        shoppersInQueue = queue['shoppers']
        if shoppersInQueue:
            complete_checkout(time, shoppersInQueue)

def run_store_simulation():
    time = 0

    initialize_queues()

    for time in range (0, 3600 * 12):
        shoppers_enter_store(time)
        shoppers_enter_queue(time)
        shoppers_checkout(time)

def extract_queue_metrics_data():
    for customer in satisfiedCustomers:
        yield {
            'id': 'queue%02d' % customer['assignedQueue'],
            'timestamp': customer['enterQueueTime'] + utcReference,
            'attrs': {
                'people_in_line': str(customer['queueLength'])
            }
        }
        yield {
            'id': 'wait%02d' % customer['assignedQueue'],
            'timestamp': customer['enterQueueTime'] + utcReference,
            'attrs': {
                'wait(sec)': str(customer['startCheckoutTime'] - customer['enterQueueTime'])
            }
        }

def get_queue_metrics_data():
    return itertools.chain(initialize_queue_metrics_data(), extract_queue_metrics_data())

def print_helpful_info():
    longestWait = 0;
    waitSum = 0;

    for customer in satisfiedCustomers:
        waitTime = customer['startCheckoutTime'] - customer['enterQueueTime']
        waitSum += waitTime
        if waitTime > longestWait:
            longestWait = waitTime
        print customer['startTime'], customer['itemCount'], customer['enterQueueTime'], customer['startCheckoutTime'], customer['finishCheckoutTime'], waitTime / 60
    print "longest wait time =", longestWait / 60, "min"
    print "average wait time =", waitSum / len(satisfiedCustomers) / 60, "min"
    print "total customers =", len(satisfiedCustomers)

def main():
    random.seed(0)
    opts = docopt(__doc__)
    if opts.get('-f'):
        output = open(opts['-f'], 'w')
    else:
        output = None

    run_store_simulation()
    #print_helpful_info()

    s = sluice.api.Sluice(output=output)
    s.update_observations(get_queue_metrics_data())

if __name__ == '__main__':
    main()

