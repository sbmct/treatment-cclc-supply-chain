"""queue location data protein generator

Usage:
  queue_data.py [-f output_filename]

Options:
  -f FILE   specify output file (defaults to pool)
"""
from docopt import docopt

import sluice
from sluice.types import v3float64, float64
from supply_chain import store_coords, time_ref

def inject_topology(queue_locs):
    locs = []

    for i in xrange(len(queue_locs)):
        queue_x, queue_y = queue_locs[i]
        #add wait icon
        wait_y = queue_y + 0
        lat, lon = store_coords.from_xy(queue_x, wait_y)
        entity = {
            'id': 'wait%02d' % (i+1),
            'kind': 'wait_time',
            'loc': v3float64(lat, lon, 0),
            'timestamp': float64(time_ref.time_start()),
        }
        locs.append(entity)
        #add queue length icon
        len_y = queue_y - 5
        lat, lon = store_coords.from_xy(queue_x, len_y)
        entity = {
            'id': 'queue%02d' % (i+1),
            'kind': 'queue_length',
            'loc': v3float64(lat, lon, 0.0),
            'timestamp': time_ref.time_start(),
        }
        locs.append(entity)

    return locs

def get_topos():
    #TODO: Can these be merged into 1? possibly add an offset to the pigment
    sep_x = 8
    queue_locs = []

    start_x = 55.954910
    start_y = 87.030631
    for i in xrange(8):
        queue_locs.append([start_x+(sep_x*i), start_y])

    start_x = 59.954910
    start_y = 61.030631
    for i in xrange(7):
        queue_locs.append([start_x+(sep_x*i), start_y])

    return inject_topology(queue_locs)

def main():
    opts = docopt(__doc__)
    if '-f' in opts:
        output = file(opts['-f'], 'w')
    else:
        output = None

    s = sluice.api.Sluice(output=output)
    s.inject_topology(get_topos())


if __name__ == '__main__':
    main()

