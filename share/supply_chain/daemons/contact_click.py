"""
s.request_webpage('contact', 'http://localhost:7787/contact/static/Dolton_IL_United_States.html', post=None, size=1.0, loc=(-0.25, 0.25), feld='right', visible=True)
                <h1>{{ name }}</h1>
                <h2>{{ id }}</h2>
                <div class="address">{{ address }}</div>
                <div class="phone">{{ phone }}</div>
                <div class="hours">{{ hours }}</div>
"""

from sluice import Sluice
import urllib

url = 'http://localhost:7787/contact/?'

class ContactFluoro(Sluice):
    edge_listen = True
    def handle_poked_it(self, p):
        ing = p.ingests()
        if ing.get('kind', None) in ('supplier', 'dist_center'):
            post = ing.get('attrs', {})
            post['id'] = ing.get('id')
            if post['id'] is not None:
                data = urllib.urlencode(post)
                self.request_webpage('contact', url+data, size=1.0, loc=(-0.25, 0.25), feld='right', visible=True)

def main():
    print "contact_click.main()"
    s = ContactFluoro()
    print "got fluoro, running"
    s.run()
    print "fluoro finished"
    s.quit()
    print "all done"

if '__main__' == __name__:
    main()

