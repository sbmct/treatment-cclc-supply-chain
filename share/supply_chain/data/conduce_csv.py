import csv
import types
import os.path
import itertools
import unicodecsv

countries = None
COMMON_KEYS = dict(name=types.StringType, city=types.StringType, state=types.StringType, country=types.StringType, latitude=types.FloatType, longitude=types.FloatType)

def standardized_country(country):
    global countries
    if countries is None:
        countriesFilename = os.path.join(os.path.dirname(__file__), 'source', 'countryCodes.csv')
        if os.path.isfile(countriesFilename):
            with open(countriesFilename, 'r') as countriesFile:
                countries = list(csv.DictReader(countriesFile))

    for codes in countries:
        if codes['A2 (ISO)'] == country.upper():
            return codes['A2 (ISO)']
        elif codes['A3 (UN)'] == country.upper():
            return codes['A2 (ISO)']
        elif codes['NUM (UN)'] == country.upper():
            return codes['A2 (ISO)']
        elif codes['DIALING CODE'] == country.upper():
            return codes['A2 (ISO)']
        elif codes['COUNTRY'] in country.upper():
            return codes['A2 (ISO)']

    return country

def compare_country(countryA, countryB):
    return standardized_country(countryA) == standardized_country(countryB)

def value_is_type(value, typeName):

    isType = False

    if typeName == types.FloatType:
        if isinstance(value, float):
            isType = True
        else:
            try:
                float(value)
                isType = True
            except ValueError:
                isType = False
    elif typeName == types.IntType:
        if isinstance(value, int):
            isType = True
        else:
            try:
                int(value)
                isType = True
            except ValueError:
                isType = False
    elif typeName == types.StringType:
        if isinstance(value, unicode):
            isType = True
        else:
            try:
                unicode(value)
                isType = True
            except ValueError:
                isType = False
    else:
        raise TypeError(str(typeName) + "not supported")


    return isType


def cast(key, value):
    if key not in COMMON_KEYS:
        return value

    typeName = COMMON_KEYS[key]
    returnValue = None

    if typeName == types.FloatType:
        if isinstance(value, float):
            returnValue = value
        else:
            returnValue = float(value)
    elif typeName == types.IntType:
        if isinstance(value, int):
            returnValue = value
        else:
            returnValue = int(value)
    elif typeName == types.StringType:
        if isinstance(value, unicode):
            returnValue = value
        else:
            returnValue = unicode(value)
    else:
        raise TypeError(str(typeName) + "not supported")

    return returnValue


def get_key_mappings(fieldNames, sampleRecord):
    commonKeys = dict(COMMON_KEYS)
    VOWELS = ['a','e','i','o','u']
    keyMapping = {}

    #These are exact matches
    for field in fieldNames:
        if field.lower() in commonKeys.keys():
            if value_is_type(sampleRecord[field], commonKeys[field.lower()]):
                keyMapping[field.lower()] = field
                del commonKeys[field.lower()]

    #These match common abbrevation patterns
    for key in commonKeys.keys():
        query1 = key[:3]
        query2 = ''.join([l for l in key if l not in VOWELS])[:3]
        for field in fieldNames:
            if query1 == field.lower() or query2 == field.lower():
                keyMapping[key] = field
            elif key in field.lower() or query1 in field.lower() or query2 in field.lower():
                keyMapping[key] = field

    #These do not match
    #TODO make these a sub-dictionary of attributes
    for field in fieldNames:
        if field not in keyMapping.itervalues():
            keyMapping[field.lower()] = field

    return keyMapping

def parse_csv(csvFile):
    reader = csv.DictReader(csvFile)
    # fileOffset = csvFile.tell()
    for row in reader:
        firstRow = row
        break
    #csvFile.seek(fileOffset)
    csvFile.seek(0)
    #numRecords = sum(1 for row in reader)
    numRecords = sum(1 for row in reader) - 1
    commonDict = [{} for i in range(numRecords)]
    keyMap = get_key_mappings(reader.fieldnames, firstRow)
    buildName = False
    #csvFile.seek(fileOffset)
    csvFile.seek(0)
    #for row, record in zip(reader, commonDict):
    for row, record in zip(itertools.islice(reader,1,None), commonDict):
        for key in keyMap:
            value = cast(key, row[keyMap[key]].decode('utf-8'))
            if key == 'country':
                value = standardized_country(value)
            record[key] = value
        if 'name' not in keyMap:
            name = ""
            if 'id' in record:
                name = record['id']
            else:
                if 'city' in record:
                    name += record['city'] + '_'
                if 'state' in record:
                    name += record['state'] + '_'
                if 'country' in record:
                    name += record['country']

            record['name'] = name
        if 'country' not in keyMap:
            #TODO: Don't assume US -- use geocoding or something
            record['country'] = standardized_country('US')

    return commonDict

def dict_to_protein(record, record_callback):
    for x in record:
        results = record_callback(x)
        #purposefully do not handle None
        for result in results:
            yield result
