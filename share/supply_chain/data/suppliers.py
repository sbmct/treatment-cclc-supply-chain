import os.path
from sluice.types import float64
import copy

def translate_record(record):
    supplier_id = record['city']
    objs = []
    obj = {
        'kind': 'supplier',
        'id': supplier_id,
        'timestamp': 0,
        'loc': [float64(record['latitude']), float64(record['longitude']), 0],
        'attrs': {
            'name': record['name'],
            'city': record['city'],
            'country': record['country']
        }
    }
    objs.append(obj)
    if float(record['longitude']) > -30:
        #put in a 2nd copy
        obj2 = copy.deepcopy(obj)
        obj2['id'] = '%s-west' % (obj2['id'])
        obj2['loc'] = [float64(record['latitude']), float64(float(record['longitude'])-360.0), 0]
        objs.append(obj2)
    return objs
