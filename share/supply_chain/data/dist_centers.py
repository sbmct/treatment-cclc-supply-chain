import csv
import os.path
import argparse
import sluice
from sluice.types import float64
import json
from datetime import datetime
from datetime import timedelta
import random
import copy

SLOWDOWN_STATES = set(('WA', 'OR', 'CA', 'NV', 'AZ', 'CO', 'UT', 'WY', 'ID', 'MT'))

def translate_record(record):
    dist_id = record['name']
    objs = []
    obj = {
        'kind': 'dist_center',
        'id': dist_id,
        'timestamp': 0.0,
        'loc': [float64(record['latitude']), float64(record['longitude']), 0],
        'attrs': {
            'name': record['contact_name'],
            'address': record['address'],
            'city': record['city'],
            'state': record['state'],
            'phone': record['phone']
        }
    }
    objs.append(obj)
    if float(record['longitude']) > -30:
        #put in a 2nd copy
        obj2 = copy.deepcopy(obj)
        obj2['id'] = '%s-west' % (obj2['id'])
        obj2['loc'] = [float64(record['latitude']), float64(float(record['longitude'])-360.0), 0]
        objs.append(obj2)
    return objs


START_WEEK = datetime(2014, 9, 1)
END_WEEK = datetime.now()
EPOCH_TIME = datetime(1970,1,1)
def epoch_time(ts):
    return (ts - EPOCH_TIME).total_seconds()

SLOWDOWN_CSV_FILE = os.path.join(os.path.dirname(__file__), '..', '..', '..', '..', 'share', 'supply_chain', 'data', 'source', 'port_delays.csv')
SLOWDOWN_DATES = None

def _read_slowdown_dates():
    global SLOWDOWN_DATES
    #read the csv
    SLOWDOWN_DATES = []
    with open(SLOWDOWN_CSV_FILE, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        reader.next()
        for row in reader:
            start_date = datetime.strptime(row[1].strip(), '%m/%d/%y')
            #the duration part is a little hackish, but hopefully real enough
            duration = row[2]
            try:
                duration = int(duration)
            except (TypeError, ValueError) as exc:
                duration = 7
            end_date = start_date + timedelta(days=duration)
            updated = False
            #a bit more up front, but merge overlapping/adjacent date ranges
            for i in xrange(len(SLOWDOWN_DATES)):
                if start_date >= SLOWDOWN_DATES[i][0] and start_date <= SLOWDOWN_DATES[i][1]:
                    updated = True
                    #use the latest end date
                    SLOWDOWN_DATES[i][1] = max(SLOWDOWN_DATES[i][1], end_date)
                    break
            if updated == False:
                SLOWDOWN_DATES.append([start_date, end_date])

def have_slowdown(mydate):
    if SLOWDOWN_DATES is None:
        _read_slowdown_dates()
    for i in xrange(len(SLOWDOWN_DATES)):
        if mydate >= SLOWDOWN_DATES[i][0] and mydate <= SLOWDOWN_DATES[i][1]:
            return True
    return False

def simulate_obs(record):
    dist_id = record['name']
    if 'stock' in record:
        for row in record['stock']:
            yield {
                'id': dist_id,
                'timestamp': row['date'],
                'attrs': { 'stock': str(row['stock']), 'delivery': str(row['delivery']), 'date': datetime.utcfromtimestamp(row['date']).strftime('%Y-%m-%d') }
            }
    else:
        mytime = START_WEEK
        while mytime < END_WEEK:
            yield {
                'id': dist_id,
                'timestamp': epoch_time(mytime),
                'attrs': { 'stock': str(max(0, min(1.5, random.gauss(1.0, 0.5)))) }
            }
            mytime += timedelta(days=7)

