from geopy import Nominatim
import csv
import argparse
import os.path
import time

def get_locations(addresses):
    geolocator = Nominatim()
    locations = []
    for address in addresses:
        print address
        idx = len(locations)
        locations.append(geolocator.geocode(address['city'] + ", " + address['country'],timeout=10))

    return locations

def write_file(addresses, locations, output_file):
    with open(output_file, 'w') as outputFile:
        writer = csv.writer(outputFile, delimiter=',')
        writer.writerow(['city','country','latitude','longitude'])
        for i in range(len(addresses)):
            if locations[i] is not None and addresses[i] is not None:
                print locations[i].latitude, locations[i].longitude
                writer.writerow([addresses[i]['city'],addresses[i]['country'],locations[i].latitude,locations[i].longitude])
            else:
                writer.writerow(['None'])

def main():
    addresses = None
    addressesFilename = os.path.join(os.path.dirname(__file__), 'overseas_suppliers.csv')
    outputFile = os.path.join(os.path.dirname(__file__), 'clean_overseas_suppliers.csv')
    if os.path.isfile(addressesFilename):
        with open(addressesFilename, 'r') as addressesFile:
            addresses = list(csv.DictReader(addressesFile))

    locations = get_locations(addresses)
    print locations
    write_file(addresses, locations)

if __name__ == '__main__':
    main()
