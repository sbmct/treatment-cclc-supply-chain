"""employee/entity paths data protein generator

Usage:
  employee_paths.py [-f output_filename]

Options:
  -f FILE   specify output file (defaults to pool)
"""
from docopt import docopt

from supply_chain.store_coords import from_pixel_xy
from supply_chain import time_ref
from sluice.api import Sluice
from sluice.types import v3float64, oblist
import datetime
import copy
import random



import os.path
from xml.dom import minidom
from svg.path import parse_path
import math


BEGIN_TIME = datetime.datetime.utcfromtimestamp(time_ref.hourly_start())
END_TIME  = datetime.datetime.utcfromtimestamp(time_ref.hourly_end())
CLICK_TIME = (time_ref.hourly_end() + time_ref.hourly_start()) / 2
BASE_ID = 36871851

SVG_FILE = os.path.join(os.path.dirname(__file__), 'source', 'retail_paths.svg')




def shoppers():
    random.seed(999)
    doc = minidom.parse(SVG_FILE)  # parseString also exists
    paths = [parse_path(path.getAttribute('d')) for path
                    in doc.getElementsByTagName('path')]
    names = [path.getAttribute('id') for path
                    in doc.getElementsByTagName('path')]
    doc.unlink()

    TIMES = {
        'customer13_just_ice_cream': [1703.07, 845.420000076],
        'customer12_queue_too_long': [1474.27, 920.220000029],
        'customer7_backdoor': [1648.8, 339.279999971],
        'customer5_leaver': [840.63, 4246.28999996],
    }
    #speeds:
    #customer13_just_ice_cream 3.05905735857
    #customer12_queue_too_long 1.55652652164
    #customer7_backdoor 3.57139000079
    #customer5_leaver 1.05393970795

    for p_i in xrange(len(paths)):
        path = paths[p_i]
        if not path:
            continue
        name = names[p_i]
        #print names[p_i]
        #continue
        #~3 pixels = 1 ft in our demo store
        #seems like 1-4 is average based on a quick google search:
        #http://www.ftpress.com/articles/article.aspx?p=1340017&seqNum=10
        path_len = path.length()
        path_len_ft = path_len * 0.32
        if name in TIMES:
            ts_start, ts_dur = TIMES[name]
            ts_start += time_ref.hourly_start()
        else:
            speed = random.uniform(1, 4)
            ts_dur = path_len / speed
            jump_in_point = random.uniform(0, 1)
            ts_start = CLICK_TIME - (ts_dur * jump_in_point)
        #ts_end = ts_start + ts_dur
        num_steps = int(math.ceil(path.length() / 20))
        num_steps = max(num_steps, 2)
        employee = '0'
        if 'employee' in names[p_i]:
            employee = '1'
        base_obj = {
            'id': 'shopper%d' % (36871851+p_i),
            'kind': 'shopper path',
            'attrs': {
                'employee': employee
            }
        }
        base_cart = {
            'id': 'cart%d' % (36871851+p_i),
            'kind': 'shopper cart',
            'attrs': {
                'checkout': '0',
                'active': '1',
                'employee': employee
            }
        }
        curr_draw_path = oblist()
        abadoned = False
        for i in xrange(num_steps+1):
            tpos = float(i)/float(num_steps)
            pos = path.point(tpos)
            svg_x = pos.real
            svg_y = pos.imag
            #TODO: Get the real transform here these are COMPLETEY made up
            lat, lon = from_pixel_xy(svg_x, svg_y, from_cropped=False)
            curr_draw_path.append(v3float64(lat, lon, 0.0))
            #if t > 0.8 and (near entrance): attrs{'bad'}
            obj = copy.deepcopy(base_obj)
            obj['timestamp'] = ts_start + (tpos * ts_dur)
            obj['path'] = copy.deepcopy(curr_draw_path)
            if tpos > 0.67 and (841<svg_x<1115) and (766<svg_y<984):
                obj['attrs']['abandon'] = '1'
                abadoned = True
            if i >= num_steps and abadoned:
                obj['attrs']['abandon'] = '2'
            yield obj

            cart = copy.deepcopy(base_cart)
            cart['timestamp'] = ts_start + (tpos * ts_dur)
            cart['loc'] = v3float64(lat, lon, 0.0)
            if (603 < svg_x < 894) and (597 < svg_y < 798):
                #cart['attrs']['checkout'] = '1'
                cart['attrs']['checkout'] = '0'
            if i >= num_steps:
                cart['attrs']['active'] = '0'
            yield cart




def main():
    opts = docopt(__doc__)
    if '-f' in opts:
        output = file(opts['-f'], 'w')
    else:
        output = None

    s = Sluice(output = output)
    s.inject_topology(shoppers())

if '__main__' == __name__:
    main()

