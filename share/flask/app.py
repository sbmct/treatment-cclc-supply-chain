from flask import Flask
from flask import render_template
import flask
import re
app = Flask(__name__)

@app.route('/<ur>/<ul>/<lower>')
def main(ur=None,ul=None,lower=None):
    return render_template('temp.html',ur=ur,ul=ul,lower = lower)

if __name__=='__main__':
    app.debug = True
    app.run()
