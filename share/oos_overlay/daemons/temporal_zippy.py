import sys, os, yaml, gzip
import cairo
from sluice import Sluice
from sluice.geometry import Box, Location, Edge
from sluice.texture import Texture, TextureFluoro
from sluice.color import color
import logging


## the official version of the librsvg bindings are apparently only distributed
## as part of the gnome-python monstrosity.  to hell with that.

## adapted from https://code.google.com/p/pychess/source/browse/lib/pychess/System/WinRsvg.py
## and http://stackoverflow.com/questions/6142757/error-with-python-ctypes-and-librsvg

from ctypes import *
import ctypes.util
import tempfile, os

class RsvgProps(Structure):
    _fields_ = [("width", c_int),
                ("height", c_int),
                ("em", c_double),
                ("ex", c_double)]

class GError(Structure):
    _fields_ = [("domain", c_uint32), ("code", c_int), ("message", c_char_p)]

class PycairoContext(Structure):
    _fields_ = [("PyObject_HEAD", c_byte * object.__basicsize__),
                ("ctx", c_void_p),
                ("base", c_void_p)]

def __load_librsvg():
    lib = CDLL(ctypes.util.find_library('rsvg-2'))
    lib.g_type_init()
    #g = CDLL(ctypes.util.find_library('gobject-2.0'))

    lib.rsvg_handle_new_from_file.argtypes = [c_char_p, POINTER(POINTER(GError))]
    lib.rsvg_handle_new_from_file.restype = c_void_p
    lib.rsvg_handle_render_cairo.argtypes = [c_void_p, c_void_p]
    lib.rsvg_handle_render_cairo.restype = c_bool
    lib.rsvg_handle_get_dimensions.argtypes = [c_void_p, POINTER(RsvgProps)]
    lib.g_object_unref.argtypes = [c_void_p]
    return lib

librsvg = __load_librsvg()

class Handle(object):
    def __init__(self, path):
        do_unlink = False
        if not isinstance(path, basestring):
            fd, fn = tempfile.mkstemp('.svg')
            f = os.fdopen(fd, 'w')
            bufsz = 1024*1024
            while True:
                try:
                    buf = path.read(bufsz)
                    if buf == '':
                        break
                except:
                    break
                f.write(buf)
            f.close()
            self.path = fn
            do_unlink = fn
        else:
            self.path = path
        err = POINTER(GError)()
        self.handle = librsvg.rsvg_handle_new_from_file(self.path, byref(err))
        if do_unlink:
            os.unlink(self.path)
        if self.handle is None:
            gerr = err.contents
            raise Exception(gerr.message)

    def get_dimensions(self):
        svgDim = RsvgProps()
        librsvg.rsvg_handle_get_dimensions(self.handle, byref(svgDim))
        return (svgDim.width, svgDim.height)

    def render_cairo(self, ctx):
        z = PycairoContext.from_address(id(ctx))
        librsvg.rsvg_handle_render_cairo(self.handle, z.ctx)

    def _remove_c_refs(self):
        if self.handle:
            librsvg.g_object_unref(self.handle)


class SVGFileHandler(object):
    def __init__(self, svgfile):
        self.svgfile = svgfile
        self.svg_handle = None

    def __enter__(self):
        self.svg_handle = Handle(self.svgfile)
        return self.svg_handle

    def __exit__(self, exc_type, exc_value, traceback):
        self.svg_handle._remove_c_refs()
        #do not return anything so exceptions continue up


class ZippyTexture(Texture):

    def scale_image(self, info, w, h):
        box = Box(Location(*info['bl']), Location(*info['tr']))

        ## Bounds size variables (with conversion from mercator)
        bw = box.tr.x - box.bl.x
        bh = self.lat2y(box.tr.lat) - self.lat2y(box.bl.lat)

        ## Calculate the points on the source image where the texture will
        ## be "cut-out" from.
        start_x = (self.bl.x - box.bl.x) / bw * w
        start_y = (self.lat2y(self.bl.lat) - self.lat2y(box.bl.lat)) / bh *h
        end_x = w + (self.tr.x - box.tr.x) / bw * w
        end_y = h + (self.lat2y(self.tr.lat) - self.lat2y(box.tr.lat)) / bh * h

        ## Destination (resulting texture) size variables
        dest_w = end_x - start_x
        dest_h = end_y - start_y

        scale_x = self.w / dest_w
        scale_y = self.h / dest_h

        ## Calculate the offsets in pixels from the top of the source image
        ## and from the left of the source image.
        top_off = (self.lat2y(self.tr.lat) - self.lat2y(box.tr.lat)) / (self.lat2y(box.tr.lat) - self.lat2y(box.bl.lat)) * h * scale_y
        left_off = (self.bl.x - box.bl.x) / (box.tr.x - box.bl.x) * w * scale_x

        self.ctx.save()
        self.ctx.translate(-left_off, top_off)
        self.ctx.scale(scale_x, scale_y)


    def load_img(self, fn):
        if self.ctx is None:
            self.init_canvas()
        if fn.endswith('.svg.gz'):
            infofn = fn[:-7]+'.yaml'
        else:
            infofn = os.path.splitext(fn)[0]+'.yaml'
        if not os.path.exists(fn):
            logging.error('Zippy source file %s not found' % fn)
            return False
        if not os.path.exists(infofn):
            logging.error('Zippy info file %s not found' % infofn)
            return False
        info = yaml.load(file(infofn))
        if fn.endswith('.svg') or fn.endswith('.svg.gz'):
            if fn.endswith('.gz'):
                svgfn = gzip.GzipFile(fn)
            else:
                svgfn = fn
            with SVGFileHandler(svgfn) as img:
                w, h = img.get_dimensions()
                self.scale_image(info, w, h)
                img.render_cairo(self.ctx)
        elif fn.endswith('.png'):
            img = cairo.ImageSurface.create_from_png(fn)
            w = img.get_width()
            h = img.get_height()
            self.scale_image(info, w, h)
            self.ctx.set_source_surface(img, 0, 0)
            self.ctx.paint()
        self.ctx.restore()
        return True

    def _generate_style(self, style):
        for ext in ('.svg.gz', '.svg', '.png'):
            fn = Sluice.get_share_file(style+ext)
            if fn is not None:
                break
        if fn is None or not self.load_img(fn):
            logging.error('Missing source data for %s' % style)
            edge = Edge(self.bl, self.tr)
            BLACK = color('black')
            self.draw_text(edge, 'Missing source data for %s' % style, color=BLACK)

    def generate(self, p):
        style = p.ingests()['style']
        self._generate_style(style)

class ZippyFluoro(TextureFluoro):
    name = 'Financials'
    texture_class = ZippyTexture




"""
Everything above this point is *mostly* copy pasted zippy.  There is a no zippy on internal-dist, but there is on dist
and there is also one on PyPI.  If pointing to internal dist, the PyPI zippy (unrelated to conduce) gets installed
and it's sort of like a silent error.  So it's easist just to put this all in here for the time being.
I made generate and _generate_style 2 separate functions - before it was all in generate().
"""
#from zippy.texture import ZippyTexture, TextureFluoro
from datetime import datetime

EPOCH_TIME = datetime(1970, 1, 1)
def epoch_time(ts):
    return (ts - EPOCH_TIME).total_seconds()
def epoch_from_string(dt):
    return epoch_time(datetime.strptime(dt, '%Y-%m-%d'))

class TemporalZippyTexture(ZippyTexture):
    def _get_time_suffix(self, time):
        if time < epoch_from_string('2014-12-01'):
            im = 0
        elif time < epoch_from_string('2014-12-02'):
            im = 1
        elif time < epoch_from_string('2014-12-03'):
            im = 2
        elif time < epoch_from_string('2014-12-04'):
            im = 3
        elif time < epoch_from_string('2014-12-05'):
            im = 4
        elif time < epoch_from_string('2014-12-06'):
            im = 5
        elif time < epoch_from_string('2014-12-07'):
            im = 6
        elif time < epoch_from_string('2014-12-08'):
            im = 7
        elif time < epoch_from_string('2014-12-09'):
            im = 8
        elif time < epoch_from_string('2014-12-10'):
            im = 9
        elif time < epoch_from_string('2014-12-19'):
            im = 10
        else:
            im = 0
        return '%02d' % im

    def generate(self, p):
        #Do some time stuff, maybe call the parent class zippy, definitely call load_img if not.
        ing = p.ingests()
        style = p.ingests()['style']
        time = ing['time']
        time_suffix = self._get_time_suffix(time)
        print style+'_'+time_suffix
        self._generate_style(style+'_'+time_suffix)

class MyZippyFluoro(TextureFluoro):
    name = 'TemporalFinancials'
    texture_class = TemporalZippyTexture

def main():
    Sluice.append_share_path(os.path.join(os.path.dirname(__file__), 'share'))
    s = MyZippyFluoro()
    s.startup()
    s.run()
    s.quit()

if '__main__' == __name__:
    main()

