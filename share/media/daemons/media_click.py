from sluice import Sluice

class MediaFluoro(Sluice):
    edge_listen = True
    def handle_poked_it(self, p):
        ing = p.ingests()
        if ing.get('kind', None) in ('tweet', 'youtube', 'flickr', 'news'):
            url = ing.get('attrs', {}).get('url', None)
            ident = ing.get('id', None)
            if url is not None and ident is not None:
                self.request_localized_webpage(ident, url, [0.8, 0.8])
                #self.request_webpage('twitter', url, size=(0.7, 1.0))
            obs = {
                    'id'        : ident,
                    'attrs': {'visited':'yes'}
                  }
            self.update_observations([obs])

def main():
    s = MediaFluoro()
    s.run()
    s.quit()

if '__main__' == __name__:
    main()

