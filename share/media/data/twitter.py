"""twitter data protein generator

Usage:
  twitter.py [-f output_filename]

Options:
  -f FILE   specify output file (defaults to pool)
"""
from docopt import docopt
import datetime
import tweepy
import csv
import dbm
import copy
import cPickle as pickle
from sluice import Sluice
from sluice.geocode import google
from loam import v3float64, oblist, float64

class StrMemo(object):
    def __init__(self, db_filename, f):
        self.__db = dbm.open(db_filename, 'c')
        self.__f = f

    def __call__(self, arg):
        if not self.__db.has_key(arg):
            self.__db.setdefault(arg, pickle.dumps(self.__f(arg)))
        return pickle.loads(self.__db.get(arg))

def epoch_time(t):
    return (t - datetime.datetime(1970,1,1)).total_seconds()

def oauth():
    consumer_key="cGtfRha0Chpt1ADd6Re7Nw"
    consumer_secret="bKspclLySwiDp5kpgmtW8MDGiYhteIgdtIiFm6v0"
    access_token="2342764706-OKg2l2O6FiVutRqECRLgKEomwfgpqwg3sA4rBFJ"
    access_token_secret="jeoCpt1Cn7JwVzHjpx87N8euuEakscw8VveCmCQ1Txfq9"
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.secure = True
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    # If the authentication was successful, you should
    # see the name of the account print out
    # print(api.me().name)
    return api

def search_twitter(api):
    # keep cache of previously geocoded User Locations
    # to reduce the number of Google API queries
    geo_lookup = StrMemo("geocache", google.google_lookup)
    # https://dev.twitter.com/overview/api/tweets
    for tweet in tweepy.Cursor(api.search,
                               q="port slowdown",
                               # geocode="lat,lon,radius"
                               # geocode="33.7683,-118.1956,1000mi",
                               since="2014-07-01",
                               until="2015-02-24",
                               lang="en").items():
        ident = tweet.id_str
        ts = epoch_time(tweet.created_at)
        txt = tweet.text
        user = tweet.user
        sn = user.screen_name
        user_loc = user.location
        user_url = "http://www.twitter.com/"+sn
        tweet_url = user_url+'/status/'+ident
        if tweet.coordinates:
            loc = tweet.coordinates['coordinates']
            lon = loc[0]
            lat = loc[1]
        else:
            # if tweet coords are not provided
            # but User Location is,
            # geocode and plot tweet there
            if len(user_loc) is not 0:
                try:
                    loc = geo_lookup(user_loc)
                    lon = loc['lng']
                    lat = loc['lat']
                except:
                    continue
            else:
                continue
        if lat and lon:
            ent = { 'timestamp' : float64(0.0),
                    'kind' : 'tweet',
                    'loc' : v3float64(lat, lon, 0.0),
                    'id' : ident,
                    'attrs' : {
                       'User Name': sn,
                       'User Profile': user_url,
                       'Tweet': txt,
                       'Tweet URL': tweet_url
                    }
                  }
            # if tweet.place is not None:
            #     ent['attrs']['Tweet Location Reference'] = tweet.place.full_name
            # longitude hack for multiple worlds
            if float(lon) > -30:
                #put in a 2nd copy
                ent2 = copy.deepcopy(ent)
                ent2['id'] = '%s-west' % (ent['id'])
                ent2['loc'] = [float64(lat), float64(float(lon)-360.0), 0]
                yield ent2
            yield ent

if '__main__' == __name__:
  opts = docopt(__doc__)
  if opts.get('-f'):
      output = file(opts['-f'], 'w')
  else:
      output = None

  if output is None:
    raise Exception('Do not call this way')

  s = Sluice(output=output)
  api = oauth()
  ents = search_twitter(api)
  s.inject_topology(ents)
