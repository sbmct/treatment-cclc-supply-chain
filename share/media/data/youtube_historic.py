"""historic geocoded youtube data protein generator

Usage:
  youtube_historic.py [-f output_filename]

Options:
  -f FILE   specify output file (defaults to pool)
"""

# INSTALLATION OF YOUTUBE'S PYTHON CLIENT API >>>> $ pip install --upgrade google-api-python-client

# YouTube API Terms >>> https://developers.google.com/youtube/terms

# Code substrate from >>> https://developers.google.com/youtube/v3/docs/search/list >>> bottom of page, Python #2 example

# "The code sample below calls the API's search.list method with q, location and locationRadius parameters
# to retrieve search results matching the provided keyword within the radius centered at a particular location.
# Using the video ids from the search result,
# the sample calls the API's videos.list method to retrieve location details of each video.

# This example uses the Python client library: https://developers.google.com/api-client-library/python/"

from docopt import docopt
import sluice
from loam import v3float64, float64
try:
  from googleapiclient.discovery import build
except ImportError:
  from apiclient.discovery import build
import datetime
import dateutil.parser
import dateutil.tz

EPOCH_TIME = datetime.datetime(1970,1,1,tzinfo=dateutil.tz.tzutc())
def epoch_time(ts):
    d = dateutil.parser.parse(ts)
    dt = d.astimezone(dateutil.tz.tzutc())
    return (dt - EPOCH_TIME).total_seconds()

def youtube_search(API_KEY):

  # Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
  # tab of
  #   https://cloud.google.com/console
  # Please ensure that you have enabled the YouTube Data API for your project.
  DEVELOPER_KEY = API_KEY
  YOUTUBE_API_SERVICE_NAME = "youtube"
  YOUTUBE_API_VERSION = "v3"

  youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
    developerKey=DEVELOPER_KEY)

  # Call the search.list method to retrieve results matching the specified
  # query term.
  # # # Parameters Described Here:
  # https://developers.google.com/youtube/v3/docs/search/list
  # query can use the Boolean NOT (-) and OR (|) operators to exclude/find videos
  # that are associated with one of several search terms.
  search_response = youtube.search().list(
    # search for videos containing these strings
    q= "port slowdown|longshoremen|port labor dispute|west coast ports",
    type= "video",
    # set Location center to Long Beach, CA
    location= "33.7683,-118.1956",
    # radius of geo-circle with center at 'location'
    locationRadius= "15000km",
    publishedAfter= "2014-08-08T00:00:00Z",
    publishedBefore= "2015-03-03T00:00:00Z",
    part= "id,snippet",
    maxResults= 25
  ).execute()

  search_videos = []

  # Merge video ids
  for search_result in search_response.get("items", []):
    search_videos.append(search_result["id"]["videoId"])
  video_ids = ",".join(search_videos)

  # Call the videos.list method to retrieve location details for each video.
  video_response = youtube.videos().list(
    id=video_ids,
    part='snippet, recordingDetails'
  ).execute()

  # print video_response.get("items", [])

  videos = []

  # Add each result to the list, and then display the list of matching videos.
  # for video_result in video_response.get("items", []):
  #   videos.append("https://www.youtube.com/watch?v=%s, %s, (%s,%s) published:%s" % (video_result["id"],
  #                             video_result["snippet"]["title"],
  #                             video_result["recordingDetails"]["location"]["latitude"],
  #                             video_result["recordingDetails"]["location"]["longitude"],
  #                             video_result["snippet"]["publishedAt"]))

  # print "Videos:\n", "\n".join(videos), "\n"

  for video_result in video_response.get("items", []):
    video = {"id"    : video_result["id"],
             "user"  : video_result["snippet"]["channelTitle"],
             "title" : video_result["snippet"]["title"],
             "lat"   : video_result["recordingDetails"]["location"]["latitude"],
             "lon"   : video_result["recordingDetails"]["location"]["longitude"],
             "upload_date" : video_result["snippet"]["publishedAt"]}
    videos.append(video)
  return videos

def build_entities(data):
  for d in xrange(len(data)):
    vid = data[d]['id']
    yield({
        'timestamp' : float64(0.0),
        'attrs'     : {
            'video' : data[d]['title'],
            'uploaded by' : data[d]['user'],
            'url' : 'https://www.youtube.com/watch?v=%s' % vid,
            'upload date' : data[d]['upload_date']
        },
        'kind' : 'youtube',
        'loc' : v3float64(data[d]['lat'], data[d]['lon'], 0.0),
        'id' : 'youtube-%s' % vid
    })

if __name__ == "__main__":

  API_KEY = "AIzaSyCpDTigKEp6GOoMIUjKO_em3Gi6eYNeQOE"

  opts = docopt(__doc__)
  if opts.get('-f'):
      output = file(opts['-f'], 'w')
  else:
      output = None

  s = sluice.Sluice(output=output)

  data = youtube_search(API_KEY)
  # prepare entities for injection
  youtube = build_entities(data)
  # inject entities
  s.inject_topology(youtube)

