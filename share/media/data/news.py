"""news data protein generator

Usage:
  news.py [-f output_filename]

Options:
  -f FILE   specify output file (defaults to pool)
"""

from docopt import docopt
import csv
import sys
import os
import datetime
import time
import copy
from sluice import Sluice
from loam import v3float64, oblist, float64

def make_timestamp(unprsdtime):
  year = (unprsdtime[:4])
  month = (unprsdtime[4:6])
  day = (unprsdtime[6:8])
  dt = datetime.datetime(year=int(year), month=int(month), day=int(day))
  ts = time.mktime(dt.timetuple())
  date = month + '-' + day + '-' + year
  return ts, date

def build_ents(csv_f):
  i = 0
  for row in csv_f:
    unprsd_time = row['Date']
    ts, date = make_timestamp(unprsd_time)
    locations = row['Locations']
    loc = locations.split('#')
    peeps = row['Persons'].split(';')
    orgs = row['Organizations'].split(';')
    ent = { 'timestamp' : float64(0.0),
            'kind' : 'news',
            'loc' : v3float64(loc[4], loc[5], 0.0),
            'id' : i,
            'attrs' : {
               'Persons': peeps,
               'Location': loc[1],
               'Organizations': orgs,
               # 'Tone': row[7],
               'Events': row['CAMEOEvents'],
               'Sources': row['Sources'],
               'url': str(row['SourceURLs'])
            }
          }
    if float(loc[5]) > -30:
      #put in a 2nd copy
      ent2 = copy.deepcopy(ent)
      ent2['id'] = '%s-west' % (ent['id'])
      ent2['loc'] = [float64(loc[4]), float64(float(loc[5])-360.0), 0]
      yield ent2
    i+=1
    yield ent

if '__main__' == __name__:

    opts = docopt(__doc__)
    if opts.get('-f'):
        output = file(opts['-f'], 'w')
    else:
        output = None

    direc = os.path.join(os.path.dirname(__file__), 'source')
    f = open(os.path.join(direc, 'portnews.csv'))
    data = csv.DictReader(f)

    s = Sluice(output=output)
    ents = build_ents(data)
    s.inject_topology(ents)

