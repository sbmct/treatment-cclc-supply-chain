"""historic geocoded flickr data protein generator

Usage:
  flicker_historic.py [-f output_filename]

Options:
  -f FILE   specify output file (defaults to pool)
"""
from docopt import docopt
import sluice
from loam import v3float64, float64
import urllib2, urllib
from cStringIO import StringIO
import xml.etree.ElementTree as ET
# optional reading on Flickr geotags: http://mashupguide.net/1.0/html/ch10s02.xhtml

def flickr_data(API_KEY):
    # searching for photos geotagged in Iraq
    # from Wed, 01 Jan 2014 18:26:30 GMT to present time
    bbox_args = [
        # additional arguments covered here:
        # https://www.flickr.com/services/api/flickr.photos.search.html
        ('api_key', API_KEY),
        ('method', 'flickr.photos.search'),

        # possible license parameters described here:
        # https://www.flickr.com/services/api/flickr.photos.licenses.getInfo.html
        ('license', '4,5,6,7,8'),

        # A free text search. Photos who's title, description or tags
        # contain the text will be returned.
        # You can exclude results that match a term by prepending it with a - character.
        # ('text', 'Baghdad'),

        # Flickr's "place_id" for Iraq is "_VDp6axTUb60wPT4Hw"
        # found here:
        # https://api.flickr.com/services/rest/?api_key=51cf5a66f721218ac5b4c75653f060af&method=flickr.places.find&query=iraq
        # ('place_id', '_VDp6axTUb60wPT4Hw'),

        ('tags', 'port, longshoremen, slowdown, labor dispute, shipping'),

        # bbox=lon0,lat0,lon1,lat1
        # lon0,lat0 = southwest corner of bbox
        # lon1,lat1 = northeast corner of bbox
        ('bbox', '-128.671875, 31.596083, -115.664062, 48.369023'),
        # ^ bbox containing the West Coast of the US

        # # number of pages to return
        # # defaults to 1
        # ('page',4),
        # # number of photos per page
        # # defaults to 250 (maximum)
        ('per_page', 50),

        # Human time (GMT): Sat, 08 Aug 2014 00:00:00 GMT
        ('min_upload_date', 1407456000.0),
        # Fri, 20 Feb 2015 22:56:54 GMT
        ('max_upload_date', 1424473014.0),

        # ('min_taken_date',1388600790),

        # minimum accuracy level you demand of the specified locations
        ('accuracy', 1),
        # extra data to include
        ('extras','date_upload, date_taken, geo, owner_name')
        # "sort (Optional)
        # The order in which to sort returned photos. Deafults to date-posted-desc
        # (unless you are doing a radial geo query, in which case
        # the default sorting is by ascending distance from the point specified).
        # The possible values are: date-posted-asc, date-posted-desc, date-taken-asc,
        # date-taken-desc, interestingness-desc, interestingness-asc, and relevance."
    ]

    bburl = 'https://api.flickr.com/services/rest/?%s' % (urllib.urlencode(bbox_args))
    # print bburl
    tree = urllib2.urlopen(bburl).read()
    root = ET.fromstring(tree)
    photos = root[0]
    # print tree

    # get metadata about the photos geotagged in Iraq
    data = []
    for x in xrange(len(photos)):
        photo = photos[x]
        meta = {'user': photo.get('owner'),
                'pid': photo.get('id'),
                'upload_date': photo.get('dateupload'),
                'date_taken': photo.get('datetaken'),
                'latitude': photo.get('latitude'),
                'longitude': photo.get('longitude')}
        data.append(meta)
        # print data
    # print "got initial flickr data"
    return data

def build_entities(API_KEY,data):
    # print "building flickr entities"
    entities = []
    # build entities for Sluice
    for d in xrange(len(data)):
        # search for a specific photo's location
        # NOTE: removed because 'geo' is added in 'extras' argument
        # of previous API call to flickr.photos.search
        # loc_args = [
        #     ('api_key', API_KEY),
        #     ('method', 'flickr.photos.geo.getLocation'),
        #     ('photo_id', pid)
        # ]
        # gurl = 'https://api.flickr.com/services/rest/?%s' % (urllib.urlencode(loc_args))
        # ploc = urllib2.urlopen(gurl).read()
        # response = ET.fromstring(ploc)
        # locs = response[0][0]
        pid = data[d]['pid']
        # lat = data[d]['latitude']
        # lon = data[d]['longitude']

        # get more info on user
        id_args = [
            ('api_key', API_KEY),
            ('method', 'flickr.people.getInfo'),
            ('user_id', data[d]['user'])
        ]
        purl = 'https://api.flickr.com/services/rest/?%s' % (urllib.urlencode(id_args))
        info = urllib2.urlopen(purl).read()
        pinfo = ET.fromstring(info)
        dossier = pinfo[0]
        if dossier.find('realname') is None:
            realname = "not provided"
        else:
            realname = dossier.find('realname').text
        if dossier.find('location') is None:
            location = "not provided"
        else:
            location = dossier.find('location').text
        photos_url = dossier.find('photosurl').text
        yield({
            'timestamp' : float64(0.0),
            'attrs'     : {
                'username' : dossier.find('username').text,
                'real_name' : realname,
                'user_location' : location,
                'user_id' : data[d]['user'],
                'profile_url' : dossier.find('profileurl').text,
                'photos_url' : photos_url,
                'photo_id' : pid,
                'upload_date' : data[d]['upload_date'],
                'date_taken' : data[d]['date_taken'],
                'url' : photos_url + pid
            },
            'kind' : 'flickr',
            'loc' : v3float64(data[d]['latitude'], data[d]['longitude'], 0.0),
            'id' : 'flickr-%s' % pid
        })

if '__main__' == __name__:

    API_KEY = "51cf5a66f721218ac5b4c75653f060af"

    opts = docopt(__doc__)
    if '-f' in opts:
        output = file(opts['-f'], 'w')
    else:
        output = None

    s = sluice.Sluice(output=output)
    # get Flickr photos/metadata geotagged in Iraq
    data = flickr_data(API_KEY)
    # get more metadata for attrs and build entities
    flickr = build_entities(API_KEY,data)
    # inject entities into Sluice
    s.inject_topology(flickr)

