from sluice.api import Sluice
import time
from datetime import datetime

WALLS = [
    '2014-12-15',
    '2014-12-31',
]
RESETS = [
    '2014-11-01',
    '2014-11-14',
    '2014-12-15',
]
EPOCH_TIME = datetime(1970,1,1)
def epoch_time(ts):
    return (datetime.strptime(ts,'%Y-%m-%d') - EPOCH_TIME).total_seconds()
#Friendler with code
WALLS = [epoch_time(wall)-1.0 for wall in WALLS]
RESETS = [epoch_time(reset)+1.0 for reset in RESETS]

class TimeLord(Sluice):
    edge_listen = False
    heart_listen = True
    hit_count = 0
    total_loops = 1
    last_hit = None
    last_time = None
    last_rate = None

    def handle_heartbeat(self, p):
        ing = p.ingests()
        ingtime = ing.get('time')
        if ingtime:
            rate = ingtime.get('rate')
            if rate != self.last_rate:
                self.last_rate = rate
                self.last_time = ingtime
                self.last_hit = None
                self.hit_count = 0
                return
            if rate is not None and rate != 0.0:
                #only get here if a rate is specified and it is not 0
                current_time = ingtime['current']
                #print "tie", current_time
                hit_limit = False
                hit_i = 0
                for i, wall in enumerate(WALLS, ):
                    if self.last_time and self.last_time < wall and current_time + rate > wall:
                        hit_limit = True
                        hit_i = i
                        break

                if hit_limit:
                    self.last_time = None
                    if self.last_hit is not None and self.last_hit == hit_i:
                        #hit the same wall
                        self.hit_count += 1
                    else:
                        self.hit_count = 1
                        self.last_hit = hit_i

                    #print self.hit_count
                    if self.hit_count < self.total_loops:
                        self.set_time(current_time, 0.0, True)
                        time.sleep(2)
                        self.set_time(RESETS[hit_i], ingtime['rate'], False)
                    else:
                        #hit the wall too much, so pause
                        stop_time = WALLS[hit_i]
                        self.set_time(stop_time, 0.0, True)
                        self.hit_count = 0
                        self.last_hit = None
                else:
                    self.last_time = current_time


def main():
    s = TimeLord()
    s.run()
    s.quit()

if '__main__' == __name__:
    main()

