import sluice
from sluice.api import Sluice
from sluice.api import Protein

class BreakOnThrough(Sluice):
    heart_listen = True
    texture_listen = False
    network_listen = False

    def __init__(self, *args, **kwargs):
        super(BreakOnThrough, self).__init__(*args, **kwargs)
        self.cur_zoom = 0
        self.map_fluoro = None
        self.store_showing = False

        self.store_name = 'franciscos_sj'
        self.min_curtain_level = 75000.0

    def run(self, *args, **kwargs):
        self.request_fluoroscopes()
        super(BreakOnThrough, self).run(*args, **kwargs)

    def check_fluoroscope(self, f):
        if f.get('name', '') == 'Map':
            self.map_fluoro = f
            cfg = self.parse_fluoroscope_config(f)
            if self.store_name in cfg.get('layers', []):
                print 'status: store is showing'
                self.store_showing = True
            else:
                print 'status: store is hidden'
                self.store_showing = False

    def handle_fluoroscope_list(self, p):
        for f in p.ingests().get('fluoroscopes', []):
            self.check_fluoroscope(f)

    def handle_fluoroscope_configuration_update(self, p):
        self.check_fluoroscope(p.ingests())

    def update_curtain_config(self, f, color = None, fade_time = None):
        cfg = f.get('curtain', [])
        if cfg != []:
            if color is not None:
                style = { 'color' : color,
                          'fade_time' : fade_time }
                f['curtain'] = style
        return f

    def show_store_layer(self):
        print 'action: showing store'
        cfg = self.parse_fluoroscope_config(self.map_fluoro)
        layers = set(cfg.get('layers', []))
        layers.add(self.store_name)
        print list(layers)
        f = self.update_fluoroscope_config(self.map_fluoro, layers=list(layers))
        f = self.update_curtain_config(f, [0.0, 0.0, 0.0, 1.0], 2.0)
        self.configure_fluoroscope(f)

    def hide_store_layer(self):
        print 'action: hiding store'
        cfg = self.parse_fluoroscope_config(self.map_fluoro)
        layers = set(cfg.get('layers', []))
        layers.discard(self.store_name)
        f = self.update_fluoroscope_config(self.map_fluoro, layers=list(layers))
        f = self.update_curtain_config(f, [0.0, 0.0, 0.0, 0.0], 2.0)
        self.configure_fluoroscope(f)

    def handle_atlas_view(self, p):
        zoom = p.ingests().get('level', 1.0)
        if self.map_fluoro:
            if zoom >= self.min_curtain_level:
                #store should be showing
                if not self.store_showing:
                    self.show_store_layer()
            elif zoom < self.min_curtain_level:
                #store should be hidden
                if self.store_showing:
                    self.hide_store_layer()
        self.cur_zoom = zoom

if '__main__' == __name__:
    s = BreakOnThrough()
    s.startup()
    s.run()
    s.quit()

