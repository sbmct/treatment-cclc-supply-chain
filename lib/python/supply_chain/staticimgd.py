import os.path
import numpy as np
from cplasma.metabolizer import Metabolizer
from cplasma import Hose, Protein

# This is just a prefix. Each image will get its own
# set of "descrips"
IMGREQ_DESCRIPS_PREFIX = ['sluice', 'prot-spec v1.0',
                          'request', 'texture']
# Another prefix. Followed by uniq image id and QID
IMGRES_DESCRIPS_PREFIX = ['sluice', 'prot-spec v1.0',
                          'response', 'texture']


def _get_filetype(filename):
    flow = filename.lower()
    if flow.endswith('jpg') or flow.endswith('jpeg'):
        filetype = 'jpg'
    elif flow.endswith('png'):
        filetype = 'png'
    else:
        filetype = 'png'
    return filetype

def main(imgdir):
    if not os.path.isdir(imgdir):
        os.makedirs(imgdir)

    metabo = Metabolizer()

    def handle_imgreq(p):
        des = p.descrips()
        daemon = des.nth(5).emit()
        print "got descrips", daemon
        if daemon.startswith('static_'):
            filename = daemon[7:]
            filepath = os.path.join(imgdir, filename)
            print filepath
            if os.path.isfile(filepath):
                ing = p.ingests().emit()
                qid = des.nth(4)
                filetype = _get_filetype(filename)
                with open(filepath, 'rb') as fp:
                    png = np.fromfile(fp, np.uint8)
                    pro = Protein(IMGRES_DESCRIPS_PREFIX + [daemon, qid], {
                        'type'  : filetype,
                        'bytes' : png,
                        'bl'    : ing['bl'],
                        'tr'    : ing['tr']
                    })
                    Hose('fluoro-to-sluice').deposit(pro)

    metabo.poolParticipate('sluice-to-fluoro')
    metabo.appendMetabolizer(IMGREQ_DESCRIPS_PREFIX,
                             handle_imgreq, 'load')

    try:
        metabo.metabolize()
    except KeyboardInterrupt:
        pass


if '__main__' == __name__:
    tmpdir = os.path.join(os.path.dirname(__file__),
                          '..', '..', '..', 'share')
    main(tmpdir)
