'''
Helper routines for store cooridate transformations

>>> lat, lon = from_xy(0, 0)
>>> lat - STORE_LAT_MIN
0.0
>>> lon - STORE_LON_MIN
0.0
>>> lat, lon = from_xy(STORE_WIDTH, STORE_HEIGHT)
>>> lat - STORE_LAT_MAX
0.0
>>> lon - STORE_LON_MAX
0.0
>>> lat, lon = from_xy(0, STORE_HEIGHT / 2.0)
>>> lat - (STORE_LAT_MIN + STORE_LAT_DELTA / 2.0)
0.0
>>> lon - STORE_LON_MIN
0.0

Some notes referring to old store2_crop.png used to derive store dims
X: ~8 to ~1086 is ~280' 1" --> ~0.32" per pixel --> 1165 image ~374 ft
Y: ~28 to ~861 is ~216' 5" --> ~0.32" per pixel --> 983 image ~315 ft

'''

STORE_LAT_MIN = 37.352826545525083
STORE_LON_MIN = -121.82658303304723

STORE_LAT_MAX = 37.355808077439782
STORE_LON_MAX = -121.82220208095123

STORE_LAT_DELTA = STORE_LAT_MAX - STORE_LAT_MIN
STORE_LON_DELTA = STORE_LON_MAX - STORE_LON_MIN

STORE_LAT_CENTER = STORE_LAT_MIN + STORE_LAT_DELTA / 2.0
STORE_LON_CENTER = STORE_LON_MIN + STORE_LON_DELTA / 2.0

STORE_WIDTH = 302.046922
STORE_HEIGHT = 254.765625

def center():
    return STORE_LAT_MIN + STORE_LAT_DELTA / 2.0, STORE_LON_MIN + STORE_LON_DELTA / 2.0

def from_xy(x, y):
    '''
    Give x/y coordinates in feet from the store itself,
    return a lat/lon pair
    '''

    lon = STORE_LON_MIN + STORE_LON_DELTA * (x / STORE_WIDTH)
    lat = STORE_LAT_MIN + STORE_LAT_DELTA * (y / STORE_HEIGHT)

    return lat, lon

def from_pixel_xy(x, y, from_cropped=True):
    #the uncropped image is 1920x983.
    #the cropped image is goes from (inclusive):
    # bl: x=406  y=958
    # tr: x=1513 y=24
    # which is of size: 1108x935
    if from_cropped == False:
        #simple, just realign
        x = x - 406
        y = y - 24
    #the cropped store has 1108 width = 374 feet
    #                  and 935 height = 315 feet
    pos_x = x / 1108.0 * STORE_WIDTH
    #y dim is flipped in pixels
    y = 935 - y
    pos_y = y / 935.0 * STORE_HEIGHT 
    return from_xy(pos_x, pos_y)



if '__main__' == __name__:
    import doctest
    doctest.testmod()
